# Node

```shell
export BLOCKCHAIN_PATH_ROOT=/home/diego_morais/blockchain
export BLOCKCHAIN_PREFERENCE_NAME=preference.json
export GRPC_PORT=28989
```

# Command line

```shell
protoc --proto_path=proto proto/*.proto --go_out=plugins=grpc:src/framework/proto_buffer
```

# Evans 
## Instalar o Evans para teste o servidor grpc 
https://github.com/ktr0731/evans

```shell
go install github.com/ktr0731/evans@latest
```

## Command Evans
evans -r -p 28989
service ServiceName


# Documeentação de como trabalhar com certificado em golang
https://www.sohamkamani.com/golang/rsa-encryption/


# Verificar se a replicação do banco foi criada
```shell
docker compose up -d
docker exec -it mongo_blockchain_first mongosh --eval "rs.status()"
```

# Configurar docker host
```shell
nano /etc/hosts
```
Adicione a linha abaixo

```
127.0.1.1       host.docker.internal
```


# Baixar o protobuf no linux
```shell
sudo apt update && sudo apt install -y protobuf-compiler && sudo apt-get install golang-goprotobuf-dev
```