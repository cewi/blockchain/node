package application

import (
	"log"
	"net"
	"os"

	"gitlab.com/cewi/blockchain/node/src/framework/controller"
	blockchain "gitlab.com/cewi/blockchain/node/src/framework/controller/blockchain"
	smith "gitlab.com/cewi/blockchain/node/src/framework/controller/smith"
	token "gitlab.com/cewi/blockchain/node/src/framework/controller/token"
	wallet "gitlab.com/cewi/blockchain/node/src/framework/controller/wallet"
	pb "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
	"gitlab.com/cewi/blockchain/node/src/use_case"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	TCP = "tcp"
)

func Init() {
	// TODO Implementar um serviço de syncronização de dados
	err2 := use_case.NewBlockchainInteractor().CreateBlockchainGenesis()
	if err2 != nil {
		panic(err2)
	}

	grpcServer := grpc.NewServer()

	pb.RegisterISignatureCommandServer(grpcServer, controller.NewSignatureCommandController())
	pb.RegisterISmithCommandServer(grpcServer, smith.NewSmithCommandController())
	pb.RegisterISmithQueryServer(grpcServer, smith.NewSmithQueryController())
	pb.RegisterIBlockchainQueryServer(grpcServer, blockchain.NewBlockchainQueryController())
	pb.RegisterIBlockchainCommandServer(grpcServer, blockchain.NewBlockchainCommandController())
	pb.RegisterIWalletCommandServer(grpcServer, wallet.NewWalletCommandController())
	pb.RegisterIWalletQueryServer(grpcServer, wallet.NewWalletQueryController())
	pb.RegisterITokenCommandServer(grpcServer, token.NewTokenCommandController())
	pb.RegisterITokenQueryServer(grpcServer, token.NewITokenQueryController())

	reflection.Register(grpcServer)
	port := os.Getenv("GRPC_PORT")

	listener, err := net.Listen(TCP, ":"+port)
	if err != nil {
		log.Fatalf(err.Error())
	}
	log.Printf("Blockchain, inicializada com sucesso na porta: %v\n", port)
	errs := grpcServer.Serve(listener)
	if errs != nil {
		panic(errs)
	}

}
