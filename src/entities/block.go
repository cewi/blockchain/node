package entities

import "gitlab.com/cewi/blockchain/node/src/utils"

type Block struct {
	Hash           string `bson:"_id"`             // Hash do bloco atual exemplo CreatedAt + PrevHash  + Smith + HashEpoch
	HashEpoch      string `bson:"hash_epoch"`      // Hash da época
	Nonce          uint64 `bson:"nonce"`           // Numero de identificação do Bloco
	CreatedAt      string `bson:"created_at"`      // Tempo que o bloco atual foi criado
	PrevHash       string `bson:"prev_hash"`       // Hash do Último bloco
	Smith          string `bson:"created_smith"`   // Hash do forjador que validou o bloco
	ValidatorSmith string `bson:"validator_smith"` // Hash do forjador que validou o bloco
	Current        bool   `bson:"current"`         // Epoca atual true
}

// GenerateHash gera o Hash do bloco atual com base em CreatedAt, PrevHash, Smith e HashEpoch.
func (b *Block) GenerateHash() {
	b.Hash, _ = utils.NewHashUtils().Generate(b.CreatedAt + b.PrevHash + b.Smith + b.HashEpoch)
}
