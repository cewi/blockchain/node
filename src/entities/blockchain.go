package entities

import (
	"log"
	"os"
	"sync"
	"time"

	"gitlab.com/cewi/blockchain/node/src/utils"
)

type Blockchain struct {
	//Uuid             primitive.ObjectID `bson:"_id"`
	Hash             string   `bson:"_id"`        // Hash da blockchain: CreatedAt + ChainID + PublicSignature
	ChainID          string   `bson:"chain_id"`   // Id da rede, exemplo: "genesis_12312341234"
	PublicSignature  string   `bson:"-"`          // chave publica para assinatura
	PrivateSignature string   `bson:"-"`          // Chave privada para assinatura
	CreatedAt        string   `bson:"created_at"` // Tempo que a rede foi criada
	Txt              float64  `bson:"txt"`        // Taxa cobrada nas transações. A taxa não poderá ser editada nos nós apenas na nó "genesis"
	Fine             uint64   `bson:"fine"`       // multa
	Reward           uint64   `bson:"reward"`
	RankOfSmith      []*Smith `bson:"rank_of_smith"` // Rank dos melhores Forjadores da rede
}

var blockchainGenesis *Blockchain

func LoadBlockchainGenesis() (*Blockchain, error) {
	var err error
	lock := &sync.Mutex{}
	if blockchainGenesis == nil {
		lock.Lock()
		defer lock.Unlock()
		var file *os.File
		fileUtils := utils.NewFileUtils()
		defer fileUtils.Close(file)

		filename := os.Getenv("BLOCKCHAIN_PATH_ROOT") + "/" + os.Getenv("BLOCKCHAIN_GENESIS")
		exists := utils.IsExists(filename)

		if exists {
			file, err = fileUtils.Open(filename)
			if err != nil {
				log.Print(err)
				file, err = fileUtils.OpenRoot("genesis.json")
				if err != nil {
					return nil, err
				}
			}
		} else {
			file, err = fileUtils.OpenRoot("genesis.json")
			if err != nil {
				return nil, err
			}
		}
		blockchainGenesis = new(Blockchain)
		_ = utils.ConvertUtils[Blockchain]{}.ToBlockchain(file, blockchainGenesis)
		timeCurrency := time.Now().UTC().String()
		blockchainGenesis.CreatedAt = timeCurrency
		blockchainGenesis.Txt = 0.000123 // TODO criar um serviço que busca Txt
		blockchainGenesis.Fine = 10      // TODO criar um serviço que busca o valor de multa a aplicação em caso de descumprimento
		blockchainGenesis.RankOfSmith = make([]*Smith, 0)
		blockchainGenesis.Hash, _ = utils.NewHashUtils().Generate(timeCurrency + blockchainGenesis.ChainID + blockchainGenesis.PublicSignature)
		blockchainGenesis.Reward = 1 // TODO criar um serviço que buscar Reward
	}
	return blockchainGenesis, nil
}
