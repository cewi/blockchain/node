package entities

import (
	"log"
	"os"
	"sync"
	"time"

	"gitlab.com/cewi/blockchain/node/src/utils"
)

type Epoch struct {
	//Uuid               primitive.ObjectID `bson:"_id"`
	Hash               string `bson:"_id"`                 // Hash da época atual exemplo: e.ChainID + e.HashBlockchain + e.CreatedAt
	ChainID            string `bson:"chain_id"`            // Id da rede, exemplo: "genesis_12312341234"
	HashBlockchain     string `bson:"hash_blockchain"`     // Hash da blocchain
	Nonce              uint64 `bson:"nonce"`               // Numero de identificação do Bloco
	PrevHash           string `bson:"prev_hash"`           // Hash da última época
	CreatedAt          string `bson:"created_at"`          // Tempo que o época atual foi criado
	TransactionsAmount uint64 `bson:"transactions_amount"` // Quantidade de transações dessa época
	Current            bool   `bson:"current"`             // Epoca atual
}

var epoch *Epoch

func LoadEpochGenesis() (*Epoch, error) {
	var err error
	lock := &sync.Mutex{}
	if epoch == nil {
		lock.Lock()
		defer lock.Unlock()
		var file *os.File
		fileUtils := utils.NewFileUtils()
		defer fileUtils.Close(file)

		filename := os.Getenv("BLOCKCHAIN_PATH_ROOT") + "/" + os.Getenv("EPOCH_GENESIS")
		exists := utils.IsExists(filename)

		if exists {
			file, err = fileUtils.Open(filename)
			if err != nil {
				log.Print(err)
				file, err = fileUtils.OpenRoot("genesis.json")
				if err != nil {
					return nil, err
				}
			}
		} else {
			file, err = fileUtils.OpenRoot("genesis.json")
			if err != nil {
				return nil, err
			}
		}
		epoch = new(Epoch)
		_ = utils.ConvertUtils[Epoch]{}.ToBlockchain(file, epoch)
	}
	timeCurrency := time.Now().UTC().String()
	epoch.CreatedAt = timeCurrency
	epoch.Current = true
	return epoch, nil
}

func (e *Epoch) GenerateHash() {
	e.Hash, _ = utils.NewHashUtils().Generate(e.CreatedAt + e.ChainID + e.HashBlockchain)
}
