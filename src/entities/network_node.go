package entities

import (
	"gitlab.com/cewi/blockchain/node/src/utils"
)

type NetworkNode struct {
	//Uuid           primitive.ObjectID `bson:"_id"`
	Hash           string `bson:"_id"`             // Hash do tblue exemplo: CreatedAt + ChainID + HashBlockchain+ Host
	ChainID        string `bson:"chain_id"`        // Id da rede, exemplo: "genesis_12312341234"
	HashBlockchain string `bson:"hash_blockchain"` // Hash da blocchain
	Host           string `bson:"host"`            // Host da cadeia atual
	Port           uint64 `bson:"port"`            // Porta da cadeia atual
	CreatedAt      string `bson:"created_at"`      // Tempo que o tblue foi criado
}

func (e *NetworkNode) GenerateHash() {
	e.Hash, _ = utils.NewHashUtils().Generate(e.CreatedAt + e.ChainID + e.HashBlockchain + e.Host)
}
