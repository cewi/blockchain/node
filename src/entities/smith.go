package entities

import "gitlab.com/cewi/blockchain/node/src/utils"

type Smith struct {
	Hash                string  `bson:"_id"`                    // Hash da época atual exemplo: CreatedAt +  ChainID + HashBlockchain + PublicSignature
	ChainID             string  `bson:"chain_id"`               // Id da rede, exemplo: "genesis_12312341234"
	HashBlockchain      string  `bson:"hash_blockchain"`        // Hash da blocchain
	PublicSignature     string  `bson:"public_signature"`       // TODO alterar esse apedido por um assinatura digital key public
	NumberOfSmithBlocks uint64  `bson:"number_of_smith_blocks"` // Total de blocos forjados
	Wallet              *Wallet `bson:"betting_wallet"`
	CreatedAt           string  `bson:"created_at"`
	MyAddress           string  `bson:"my_address"`
}

func (e *Smith) GenerateHash() {
	e.Hash, _ = utils.NewHashUtils().Generate(e.CreatedAt + e.ChainID + e.HashBlockchain + e.PublicSignature)
}
