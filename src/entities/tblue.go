package entities

import "gitlab.com/cewi/blockchain/node/src/utils"

// Tanzanita Azul é a 1° pedra preciosa mais rara do mundo, criado o apelido Tblue
type Tblue struct {
	Hash           string `bson:"_id"`             // Hash do gblue exemplo: CreatedAt + Address + Serial + HashToken + ChainID +  HashBlockchain
	ChainID        string `bson:"chain_id"`        // Id da rede, exemplo: "genesis_12312341234"
	HashBlockchain string `bson:"hash_blockchain"` // Hash da blocchain
	HashToken      string `bson:"hash_token"`      // Hash do token
	Address        string `bson:"address"`         // Endereço do gblue
	Serial         string `bson:"serial"`          // Serial do gblue informado pelo usuario (Opcional)
	PrevHash       string `bson:"prev_hash"`       // Hash do gblue anterior
	CreatedAt      string `bson:"created_at"`      // Tempo que o gblue foi criado
	Balance        string `bson:"balance"`         // balanço total do gblue
}

func (e *Tblue) GenerateHash() {
	e.Hash, _ = utils.NewHashUtils().Generate(e.CreatedAt + e.Address + e.Serial + e.HashToken + e.ChainID + e.HashBlockchain)
}
