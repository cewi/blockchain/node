package entities

import "gitlab.com/cewi/blockchain/node/src/utils"

type Token struct {
	Hash           string `bson:"_id"`             // Hash do token exemplo:  CreatedAt + ChainID + HashBlockchain + Nickname
	ChainID        string `bson:"chain_id"`        // Id da rede, exemplo: "genesis_12312341234"
	HashBlockchain string `bson:"hash_blockchain"` // Hash da blocchain
	Nickname       string `bson:"nickname"`        // Apelido do token
	Acronym        string `bson:"acronym"`         // sigla do token ex.: ADA, ETH
	CreatedAt      string `bson:"created_at"`      // Tempo que o token foi criado
	Balance        string `bson:"balance"`         // balanço total do tblue é a soma de tblue criando
}

func (e *Token) GenerateHash() {
	e.Hash, _ = utils.NewHashUtils().Generate(e.CreatedAt + e.ChainID + e.HashBlockchain + e.Nickname)
}
