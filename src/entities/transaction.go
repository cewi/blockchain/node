package entities

import (
  "fmt"

  "gitlab.com/cewi/blockchain/node/src/utils"
)

type Transaction struct {
	Hash            string  `bson:"_id"` // Hash da transação: CreatedAt + SenderAddress + AddressReceiver + Amount + HashBlock + HashEpoch
	HashBlock       string  `bson:"hash_block"`
	HashEpoch       string  `bson:"hash_epoch"`
	SenderAddress   string  `bson:"sender_address"`   // Remetente é quem envia a quantidade
	AddressReceiver string  `bson:"address_receiver"` // Destinatário que recebe a quantidade
	SenderBalance   string  `bson:"sender_balance"`   // Saldo da carteira do remetente
	Amount          float64 `bson:"amount"`           // A quantidade do token
	AmountRate      float64 `bson:"amount_rate"`      // A quantidade com a taxa de transferencia
	Rate            float64 `bson:"rate"`             // taxa de transferencia
	CreatedAt       string  `bson:"created_at"`       // Tempo que a transação foi criada
}

func (t *Transaction) GenerateHash() {
	t.Hash, _ = utils.NewHashUtils().Generate(
		t.CreatedAt + t.SenderAddress + t.AddressReceiver + fmt.Sprintf(
			"%v",
			t.Amount,
		) + t.HashBlock + t.HashEpoch,
	)
}
