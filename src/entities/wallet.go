package entities

type Wallet struct {
	Address  string      `bson:"_id"`     // Endereço da cartaira do forjador
	Balance  string      `bson:"balance"` // Aposta que o forjador usa para aumenta as possibilidades de forja novos blocos
	MyTokens []*MyTokens `bson:"tokens"`
}

type MyTokens struct {
	Balance        string `bson:"balance"` // Aposta que o forjador usa para aumenta as possibilidades de forja novos blocos
	Nickname       string `bson:"nickname"`
	ChainID        string `bson:"chain_id"`        // Id da rede, exemplo: "genesis_12312341234"
	HashBlockchain string `bson:"hash_blockchain"` // Hash da blocchain
}
