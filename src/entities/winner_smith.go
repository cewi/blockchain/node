package entities

type WinnerSmith struct {
	ID              string `bson:"_id"`
	Hash            string `bson:"hash"`
	CreatedAt       string `bson:"created_at"` // Tempo que a transação foi criada
	PublicSignature string `bson:"public_signature"`
}
