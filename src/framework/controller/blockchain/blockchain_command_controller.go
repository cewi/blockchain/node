package controller

import (
	"context"
	"sync"

	"gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"

	"gitlab.com/cewi/blockchain/node/src/use_case"
)

type blockchainCommandController struct {
	blockchainInteractor use_case.IBlockchainInteractor
}

var controllerCommand pb.IBlockchainCommandServer

func NewBlockchainCommandController() pb.IBlockchainCommandServer {
	lock := &sync.Mutex{}
	if controllerCommand == nil {
		lock.Lock()
		defer lock.Unlock()
		controllerCommand = &blockchainCommandController{
			blockchainInteractor: use_case.NewBlockchainInteractor(),
		}
	}
	return controllerCommand
}

func (b *blockchainCommandController) CreateNewBlock(_ context.Context, req *pb.BlockchainRequest) (*pb.BlockchainResponse, error) {
	return b.blockchainInteractor.CreateNewBlock(req)
}

func (b *blockchainCommandController) RegisterNode(_ context.Context, request *pb.RegisterNodeRequest) (*pb.ResponseCommand, error) {
	//return b.blockchainInteractor.RegisterNode(request)
	return nil, nil
}
