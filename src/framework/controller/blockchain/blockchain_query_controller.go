package controller

import (
	"context"
	"sync"

	pb "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
	"gitlab.com/cewi/blockchain/node/src/framework/repository/blockchain"
	"gitlab.com/cewi/blockchain/node/src/use_case"
)

type BlockchainQueryController struct {
	repository repository.IBlockchainQueryRepository
}

var controllerQuery pb.IBlockchainQueryServer

func NewBlockchainQueryController() pb.IBlockchainQueryServer {
	lock := &sync.Mutex{}
	if controllerQuery == nil {
		lock.Lock()
		defer lock.Unlock()
		controllerQuery = &BlockchainQueryController{
			repository: repository.NewBlockchainQueryRepository(),
		}
	}
	return controllerQuery
}

func (b *BlockchainQueryController) DoesHostAlreadyExist(_ context.Context, request *pb.HostRequest) (*pb.QueryResponse, error) {
	// FIXME implementar esse método
	//return b.token.IsNodeWithHostExist(request.Host)
	return nil, nil
}

func (b *BlockchainQueryController) DoesChainIdAlreadyExist(_ context.Context, request *pb.ChainIdRequest) (*pb.QueryResponse, error) {
	// FIXME implementar esse método
	//return b.token.IsNodeWithChainIDExist(request.ChainID)
	return nil, nil
}

func (b *BlockchainQueryController) FindAllNetworkNodes(_ context.Context, request *pb.FindAllRequest) (*pb.FindAllResponse, error) {
	// FIXME implementar esse método
	//return b.token.FindAllNetworkNodes(request)
	return nil, nil
}

func (b *BlockchainQueryController) GetChainIdGenesis(_ context.Context, req *pb.ChainIdRequestEmpty) (*pb.ChainIdResponse, error) {
	return b.repository.GetChainIdGenesis(req, use_case.ChainID)
}
