package controller

import (
  "context"
  "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
  "gitlab.com/cewi/blockchain/node/src/use_case"
  "sync"
)

var iSignatureCommand pb.ISignatureCommandServer

type signatureCommandController struct {
  signatureInteractor use_case.ISignatureInteractor
}

func NewSignatureCommandController() pb.ISignatureCommandServer {
  lock := &sync.Mutex{}
  if iSignatureCommand == nil {
    lock.Lock()
    defer lock.Unlock()
    iSignatureCommand = &signatureCommandController{
      signatureInteractor: use_case.NewSignatureInteractor(),
    }
  }
  return iSignatureCommand
}

func (s signatureCommandController) GenerateSignature(_ context.Context, _ *pb.Empty) (*pb.SignatureResponse, error) {
  return s.signatureInteractor.GenerateSignature()
}
