package controller

import (
	"context"
	"sync"

	pb "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
	"gitlab.com/cewi/blockchain/node/src/use_case"
)

type smithCommandController struct {
	smithInteractor use_case.ISmithInteractor
}

var smithCommand pb.ISmithCommandServer

func NewSmithCommandController() pb.ISmithCommandServer {
	lock := &sync.Mutex{}
	if smithCommand == nil {
		lock.Lock()
		defer lock.Unlock()
		smithCommand = &smithCommandController{
			smithInteractor: use_case.NewSmithInteractor(),
		}
	}
	return smithCommand
}

func (s *smithCommandController) RegisterSmith(_ context.Context, req *pb.SmithRequest) (*pb.SmithResponseCommand, error) {
	return s.smithInteractor.RegisterSmith(req)
}

func (s *smithCommandController) ToStopBeingForger(_ context.Context, req *pb.SmithRemoveRequest) (*pb.SmithResponseCommand, error) {
	return s.smithInteractor.ToStopBeingSmith(req)
}

func (s *smithCommandController) SendFine(_ context.Context, req *pb.SmithFineRequest) (*pb.SmithResponseCommand, error) {
	return s.smithInteractor.SendFine(req)
}
