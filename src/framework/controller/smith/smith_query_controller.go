package controller

import (
	"context"
	"sync"

	pb "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
	repository "gitlab.com/cewi/blockchain/node/src/framework/repository/smith"
	"gitlab.com/cewi/blockchain/node/src/use_case"
)

type smithQueryController struct {
	smithRepository repository.ISmithQueryRepository
	smithInteractor use_case.ISmithInteractor
}

var smithQuery pb.ISmithQueryServer

func NewSmithQueryController() pb.ISmithQueryServer {
	lock := &sync.Mutex{}
	if smithQuery == nil {
		lock.Lock()
		defer lock.Unlock()
		smithQuery = &smithQueryController{
			smithRepository: repository.NewSmithQueryRepository(),
			smithInteractor: use_case.NewSmithInteractor(),
		}
	}
	return smithQuery
}

func (c *smithQueryController) FindAll(_ context.Context, req *pb.SmithAllRequest) (*pb.SmithAllReponse, error) {
	return c.smithRepository.FindAllPage(req.Page, req.Size, req.ChainId, req.HashBlockchain)
}

func (c *smithQueryController) CheckWinner(_ context.Context, req *pb.SmithCheckRequest) (*pb.SmithCheckResponse, error) {
	return c.smithInteractor.CheckWinner(req.Hash)
}

func (c *smithQueryController) GetBalance(_ context.Context, req *pb.SmithCheckRequest) (*pb.SmithBalanceResponse, error) {
	return c.smithInteractor.GetBalance(req)
}
