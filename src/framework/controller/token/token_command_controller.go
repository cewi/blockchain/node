package controller

import (
	"context"
	"sync"

	pb "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
	"gitlab.com/cewi/blockchain/node/src/use_case"
)

type tokenController struct {
	tokenInteractor use_case.ITokenInteractor
}

var iTokenController pb.ITokenCommandServer

func NewTokenCommandController() pb.ITokenCommandServer {
	lock := &sync.Mutex{}
	if iTokenController == nil {
		lock.Lock()
		defer lock.Unlock()

		iTokenController = &tokenController{
			tokenInteractor: use_case.NewTokenInteractor(),
		}

	}
	return iTokenController
}

func (t tokenController) Create(_ context.Context, req *pb.TokenRequest) (*pb.TokenResponse, error) {
	return t.tokenInteractor.Create(req)
}
