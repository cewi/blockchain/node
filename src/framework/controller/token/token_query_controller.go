package controller

import (
	"context"
	"sync"

	pb "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
	repository "gitlab.com/cewi/blockchain/node/src/framework/repository/token"
)

type tokenQueryController struct {
	tokenQueryRepository repository.ITokenQueryRepository
}

var iTokenQueryController pb.ITokenQueryServer

func NewITokenQueryController() pb.ITokenQueryServer {
	lock := &sync.Mutex{}
	if iTokenQueryController == nil {
		lock.Lock()
		defer lock.Unlock()
		iTokenQueryController = &tokenQueryController{
			tokenQueryRepository: repository.NewTokenQueryRepository(),
		}
	}
	return iTokenQueryController
}

func (t tokenQueryController) FindALl(_ context.Context, req *pb.TokensRequest) (*pb.TokensResponse, error) {
	return t.tokenQueryRepository.FindByAll(req)
}
