package controller

import (
	"context"
	"sync"

	pb "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
	"gitlab.com/cewi/blockchain/node/src/use_case"
)

var iWalletCommandController pb.IWalletCommandServer

type walletCommandController struct {
	walletInteractor use_case.IWalletInteractor
}

func NewWalletCommandController() pb.IWalletCommandServer {
	lock := &sync.Mutex{}
	if iWalletCommandController == nil {
		lock.Lock()
		defer lock.Unlock()
		iWalletCommandController = &walletCommandController{
			walletInteractor: use_case.NewWalletInteractor(),
		}
	}
	return iWalletCommandController
}

func (w *walletCommandController) Create(_ context.Context, req *pb.WalletRequest) (*pb.WalletReponse, error) {
	return w.walletInteractor.Create(req)
}

func (w *walletCommandController) NewAddress(_ context.Context, empty *pb.AddressRequestEmpty) (*pb.AddressResponse, error) {
	return w.walletInteractor.NewAddress()
}
