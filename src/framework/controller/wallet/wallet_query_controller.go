package controller

import (
	"context"
	"sync"

	pb "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
	"gitlab.com/cewi/blockchain/node/src/use_case"
)

var iWalletQueryController pb.IWalletQueryServer

type walletQueryController struct {
	walletInteractor use_case.IWalletInteractor
}

func NewWalletQueryController() pb.IWalletQueryServer {
	lock := &sync.Mutex{}
	if iWalletQueryController == nil {
		lock.Lock()
		defer lock.Unlock()
		iWalletQueryController = &walletQueryController{
			walletInteractor: use_case.NewWalletInteractor(),
		}
	}
	return iWalletQueryController
}

func (w walletQueryController) GetBalance(_ context.Context, req *pb.BalanceRequest) (*pb.BalanceResponse, error) {
	return w.walletInteractor.GetBalance(req)
}
