package repository_database

import (
  "context"
  "log"
  "os"
  "strings"
  "sync"

  "go.mongodb.org/mongo-driver/mongo"
  "go.mongodb.org/mongo-driver/mongo/options"
)

type IMongoDataSource interface {
  Connect() (*mongo.Client, error)
  Close(connect *mongo.Client)
  DataSource(client *mongo.Client, database, collection string) *mongo.Collection
  Ping(client *mongo.Client) error
}

type mongoDataSource struct {
  clientOptions *options.ClientOptions
}

var dataSource IMongoDataSource

func initClientOptions() *options.ClientOptions {
  uri := os.Getenv("DATABASE_MONGODB_URL")
  if strings.EqualFold("dev", os.Getenv("PROFILE")) {
    return options.Client().ApplyURI(uri).SetDirect(true)
  }
  return options.Client().ApplyURI(uri)
}

func (c *mongoDataSource) Connect() (*mongo.Client, error) {
  ctx := context.Background()
  client, err := mongo.Connect(ctx, c.clientOptions)
  if err != nil {
    log.Println(err)
    return nil, err
  }
  return client, nil
}

func (c *mongoDataSource) Ping(client *mongo.Client) error {
  err := client.Ping(context.Background(), nil)
  if err != nil {
    log.Println(err)
    return err
  }
  return nil
}

func (c *mongoDataSource) Close(connect *mongo.Client) {
  defer func(client *mongo.Client) {
    err := client.Disconnect(context.Background())
    if err != nil {
      panic(err)
    }
  }(connect)
}

func (c *mongoDataSource) DataSource(client *mongo.Client, database, collection string) *mongo.Collection {
  return client.Database(database).Collection(collection)
}

func NewMongoDataSource() IMongoDataSource {
  lock := &sync.Mutex{}
  if dataSource == nil {
    lock.Lock()
    defer lock.Unlock()

    dataSource = &mongoDataSource{clientOptions: initClientOptions()}
    connect, err := dataSource.Connect()
    if err != nil {
      log.Println(err)
      return nil
    }
    err = dataSource.Ping(connect)
    if err != nil {
      log.Println(err)
      return nil
    }
    defer dataSource.Close(connect)
  }
  return dataSource
}
