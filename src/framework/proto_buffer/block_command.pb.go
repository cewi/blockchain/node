// Code generated by protoc-gen-go. DO NOT EDIT.
// source: block_command.proto

package pb

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type BlockCreateRequest struct {
	ForceHash            string   `protobuf:"bytes,1,opt,name=ForceHash,proto3" json:"ForceHash,omitempty"`
	Signature            string   `protobuf:"bytes,2,opt,name=Signature,proto3" json:"Signature,omitempty"`
	Data                 string   `protobuf:"bytes,3,opt,name=Data,proto3" json:"Data,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *BlockCreateRequest) Reset()         { *m = BlockCreateRequest{} }
func (m *BlockCreateRequest) String() string { return proto.CompactTextString(m) }
func (*BlockCreateRequest) ProtoMessage()    {}
func (*BlockCreateRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_1078a509f9b28667, []int{0}
}

func (m *BlockCreateRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BlockCreateRequest.Unmarshal(m, b)
}
func (m *BlockCreateRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BlockCreateRequest.Marshal(b, m, deterministic)
}
func (m *BlockCreateRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BlockCreateRequest.Merge(m, src)
}
func (m *BlockCreateRequest) XXX_Size() int {
	return xxx_messageInfo_BlockCreateRequest.Size(m)
}
func (m *BlockCreateRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_BlockCreateRequest.DiscardUnknown(m)
}

var xxx_messageInfo_BlockCreateRequest proto.InternalMessageInfo

func (m *BlockCreateRequest) GetForceHash() string {
	if m != nil {
		return m.ForceHash
	}
	return ""
}

func (m *BlockCreateRequest) GetSignature() string {
	if m != nil {
		return m.Signature
	}
	return ""
}

func (m *BlockCreateRequest) GetData() string {
	if m != nil {
		return m.Data
	}
	return ""
}

type BlockCreateResponse struct {
	Status               string   `protobuf:"bytes,1,opt,name=Status,proto3" json:"Status,omitempty"`
	Message              string   `protobuf:"bytes,2,opt,name=Message,proto3" json:"Message,omitempty"`
	Error                string   `protobuf:"bytes,3,opt,name=Error,proto3" json:"Error,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *BlockCreateResponse) Reset()         { *m = BlockCreateResponse{} }
func (m *BlockCreateResponse) String() string { return proto.CompactTextString(m) }
func (*BlockCreateResponse) ProtoMessage()    {}
func (*BlockCreateResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_1078a509f9b28667, []int{1}
}

func (m *BlockCreateResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BlockCreateResponse.Unmarshal(m, b)
}
func (m *BlockCreateResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BlockCreateResponse.Marshal(b, m, deterministic)
}
func (m *BlockCreateResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BlockCreateResponse.Merge(m, src)
}
func (m *BlockCreateResponse) XXX_Size() int {
	return xxx_messageInfo_BlockCreateResponse.Size(m)
}
func (m *BlockCreateResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_BlockCreateResponse.DiscardUnknown(m)
}

var xxx_messageInfo_BlockCreateResponse proto.InternalMessageInfo

func (m *BlockCreateResponse) GetStatus() string {
	if m != nil {
		return m.Status
	}
	return ""
}

func (m *BlockCreateResponse) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func (m *BlockCreateResponse) GetError() string {
	if m != nil {
		return m.Error
	}
	return ""
}

func init() {
	proto.RegisterType((*BlockCreateRequest)(nil), "BlockCreateRequest")
	proto.RegisterType((*BlockCreateResponse)(nil), "BlockCreateResponse")
}

func init() {
	proto.RegisterFile("block_command.proto", fileDescriptor_1078a509f9b28667)
}

var fileDescriptor_1078a509f9b28667 = []byte{
	// 215 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x12, 0x4e, 0xca, 0xc9, 0x4f,
	0xce, 0x8e, 0x4f, 0xce, 0xcf, 0xcd, 0x4d, 0xcc, 0x4b, 0xd1, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x57,
	0x4a, 0xe1, 0x12, 0x72, 0x02, 0x09, 0x3b, 0x17, 0xa5, 0x26, 0x96, 0xa4, 0x06, 0xa5, 0x16, 0x96,
	0xa6, 0x16, 0x97, 0x08, 0xc9, 0x70, 0x71, 0xba, 0xe5, 0x17, 0x25, 0xa7, 0x7a, 0x24, 0x16, 0x67,
	0x48, 0x30, 0x2a, 0x30, 0x6a, 0x70, 0x06, 0x21, 0x04, 0x40, 0xb2, 0xc1, 0x99, 0xe9, 0x79, 0x89,
	0x25, 0xa5, 0x45, 0xa9, 0x12, 0x4c, 0x10, 0x59, 0xb8, 0x80, 0x90, 0x10, 0x17, 0x8b, 0x4b, 0x62,
	0x49, 0xa2, 0x04, 0x33, 0x58, 0x02, 0xcc, 0x56, 0x8a, 0xe5, 0x12, 0x46, 0xb1, 0xa5, 0xb8, 0x20,
	0x3f, 0xaf, 0x38, 0x55, 0x48, 0x8c, 0x8b, 0x2d, 0xb8, 0x24, 0xb1, 0xa4, 0xb4, 0x18, 0x6a, 0x07,
	0x94, 0x27, 0x24, 0xc1, 0xc5, 0xee, 0x9b, 0x5a, 0x5c, 0x9c, 0x98, 0x0e, 0x33, 0x1e, 0xc6, 0x15,
	0x12, 0xe1, 0x62, 0x75, 0x2d, 0x2a, 0xca, 0x2f, 0x82, 0x9a, 0x0e, 0xe1, 0x18, 0xf9, 0x71, 0xf1,
	0x7a, 0x42, 0xcc, 0x87, 0xf8, 0x4d, 0xc8, 0x96, 0x8b, 0x0f, 0x62, 0x95, 0x5f, 0x6a, 0x39, 0x58,
	0x42, 0x48, 0x58, 0x0f, 0xd3, 0x9b, 0x52, 0x22, 0x7a, 0x58, 0x5c, 0xa5, 0xc4, 0xe0, 0xc4, 0x12,
	0xc5, 0x54, 0x90, 0x94, 0xc4, 0x06, 0x0e, 0x21, 0x63, 0x40, 0x00, 0x00, 0x00, 0xff, 0xff, 0x58,
	0x05, 0x67, 0x72, 0x38, 0x01, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// IBlockCommandClient is the client API for IBlockCommand service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type IBlockCommandClient interface {
	CreateNewBlock(ctx context.Context, in *BlockCreateRequest, opts ...grpc.CallOption) (*BlockCreateResponse, error)
}

type iBlockCommandClient struct {
	cc grpc.ClientConnInterface
}

func NewIBlockCommandClient(cc grpc.ClientConnInterface) IBlockCommandClient {
	return &iBlockCommandClient{cc}
}

func (c *iBlockCommandClient) CreateNewBlock(ctx context.Context, in *BlockCreateRequest, opts ...grpc.CallOption) (*BlockCreateResponse, error) {
	out := new(BlockCreateResponse)
	err := c.cc.Invoke(ctx, "/IBlockCommand/CreateNewBlock", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// IBlockCommandServer is the server API for IBlockCommand service.
type IBlockCommandServer interface {
	CreateNewBlock(context.Context, *BlockCreateRequest) (*BlockCreateResponse, error)
}

// UnimplementedIBlockCommandServer can be embedded to have forward compatible implementations.
type UnimplementedIBlockCommandServer struct {
}

func (*UnimplementedIBlockCommandServer) CreateNewBlock(ctx context.Context, req *BlockCreateRequest) (*BlockCreateResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateNewBlock not implemented")
}

func RegisterIBlockCommandServer(s *grpc.Server, srv IBlockCommandServer) {
	s.RegisterService(&_IBlockCommand_serviceDesc, srv)
}

func _IBlockCommand_CreateNewBlock_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BlockCreateRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IBlockCommandServer).CreateNewBlock(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/IBlockCommand/CreateNewBlock",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IBlockCommandServer).CreateNewBlock(ctx, req.(*BlockCreateRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _IBlockCommand_serviceDesc = grpc.ServiceDesc{
	ServiceName: "IBlockCommand",
	HandlerType: (*IBlockCommandServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateNewBlock",
			Handler:    _IBlockCommand_CreateNewBlock_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "block_command.proto",
}
