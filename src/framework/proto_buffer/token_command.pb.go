// Code generated by protoc-gen-go. DO NOT EDIT.
// source: token_command.proto

package pb

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type TokenRequest struct {
	ChainID              string   `protobuf:"bytes,1,opt,name=ChainID,proto3" json:"ChainID,omitempty"`
	HashBlockchain       string   `protobuf:"bytes,2,opt,name=HashBlockchain,proto3" json:"HashBlockchain,omitempty"`
	Nickname             string   `protobuf:"bytes,3,opt,name=Nickname,proto3" json:"Nickname,omitempty"`
	Acronym              string   `protobuf:"bytes,4,opt,name=Acronym,proto3" json:"Acronym,omitempty"`
	Balance              float64  `protobuf:"fixed64,5,opt,name=Balance,proto3" json:"Balance,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *TokenRequest) Reset()         { *m = TokenRequest{} }
func (m *TokenRequest) String() string { return proto.CompactTextString(m) }
func (*TokenRequest) ProtoMessage()    {}
func (*TokenRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_14d267b1229ff1f7, []int{0}
}

func (m *TokenRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TokenRequest.Unmarshal(m, b)
}
func (m *TokenRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TokenRequest.Marshal(b, m, deterministic)
}
func (m *TokenRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TokenRequest.Merge(m, src)
}
func (m *TokenRequest) XXX_Size() int {
	return xxx_messageInfo_TokenRequest.Size(m)
}
func (m *TokenRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_TokenRequest.DiscardUnknown(m)
}

var xxx_messageInfo_TokenRequest proto.InternalMessageInfo

func (m *TokenRequest) GetChainID() string {
	if m != nil {
		return m.ChainID
	}
	return ""
}

func (m *TokenRequest) GetHashBlockchain() string {
	if m != nil {
		return m.HashBlockchain
	}
	return ""
}

func (m *TokenRequest) GetNickname() string {
	if m != nil {
		return m.Nickname
	}
	return ""
}

func (m *TokenRequest) GetAcronym() string {
	if m != nil {
		return m.Acronym
	}
	return ""
}

func (m *TokenRequest) GetBalance() float64 {
	if m != nil {
		return m.Balance
	}
	return 0
}

type TokenResponse struct {
	Status               string   `protobuf:"bytes,1,opt,name=Status,proto3" json:"Status,omitempty"`
	Message              string   `protobuf:"bytes,2,opt,name=Message,proto3" json:"Message,omitempty"`
	HashToken            string   `protobuf:"bytes,3,opt,name=HashToken,proto3" json:"HashToken,omitempty"`
	Nickname             string   `protobuf:"bytes,4,opt,name=Nickname,proto3" json:"Nickname,omitempty"`
	Acronym              string   `protobuf:"bytes,5,opt,name=Acronym,proto3" json:"Acronym,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *TokenResponse) Reset()         { *m = TokenResponse{} }
func (m *TokenResponse) String() string { return proto.CompactTextString(m) }
func (*TokenResponse) ProtoMessage()    {}
func (*TokenResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_14d267b1229ff1f7, []int{1}
}

func (m *TokenResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TokenResponse.Unmarshal(m, b)
}
func (m *TokenResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TokenResponse.Marshal(b, m, deterministic)
}
func (m *TokenResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TokenResponse.Merge(m, src)
}
func (m *TokenResponse) XXX_Size() int {
	return xxx_messageInfo_TokenResponse.Size(m)
}
func (m *TokenResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_TokenResponse.DiscardUnknown(m)
}

var xxx_messageInfo_TokenResponse proto.InternalMessageInfo

func (m *TokenResponse) GetStatus() string {
	if m != nil {
		return m.Status
	}
	return ""
}

func (m *TokenResponse) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func (m *TokenResponse) GetHashToken() string {
	if m != nil {
		return m.HashToken
	}
	return ""
}

func (m *TokenResponse) GetNickname() string {
	if m != nil {
		return m.Nickname
	}
	return ""
}

func (m *TokenResponse) GetAcronym() string {
	if m != nil {
		return m.Acronym
	}
	return ""
}

func init() {
	proto.RegisterType((*TokenRequest)(nil), "TokenRequest")
	proto.RegisterType((*TokenResponse)(nil), "TokenResponse")
}

func init() {
	proto.RegisterFile("token_command.proto", fileDescriptor_14d267b1229ff1f7)
}

var fileDescriptor_14d267b1229ff1f7 = []byte{
	// 255 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x74, 0x91, 0xb1, 0x4e, 0xc3, 0x30,
	0x10, 0x86, 0xe5, 0x92, 0x06, 0x7a, 0x22, 0x1d, 0x8c, 0x84, 0xac, 0x8a, 0xa1, 0xea, 0x00, 0x9d,
	0x32, 0xc0, 0xc2, 0x4a, 0xc2, 0x40, 0x07, 0x18, 0x02, 0x13, 0x0b, 0xba, 0x9a, 0x13, 0xad, 0x52,
	0xdb, 0x21, 0x76, 0x07, 0x1e, 0x84, 0x9d, 0x47, 0x45, 0x76, 0x1d, 0x20, 0x48, 0x1d, 0xbf, 0xdf,
	0xe7, 0x5f, 0x9f, 0xee, 0xe0, 0xc4, 0x99, 0x9a, 0xf4, 0x8b, 0x34, 0x4a, 0xa1, 0x7e, 0xcd, 0x9b,
	0xd6, 0x38, 0x33, 0xfb, 0x62, 0x70, 0xfc, 0xe4, 0xf3, 0x8a, 0xde, 0xb7, 0x64, 0x1d, 0x17, 0x70,
	0x58, 0xae, 0x70, 0xad, 0x17, 0xb7, 0x82, 0x4d, 0xd9, 0x7c, 0x54, 0x75, 0xc8, 0xcf, 0x61, 0x7c,
	0x87, 0x76, 0x55, 0x6c, 0x8c, 0xac, 0xa5, 0xcf, 0xc4, 0x20, 0x0c, 0xfc, 0x4b, 0xf9, 0x04, 0x8e,
	0x1e, 0xd6, 0xb2, 0xd6, 0xa8, 0x48, 0x1c, 0x84, 0x89, 0x1f, 0xf6, 0xed, 0x37, 0xb2, 0x35, 0xfa,
	0x43, 0x89, 0x64, 0xd7, 0x1e, 0xd1, 0xbf, 0x14, 0xb8, 0x41, 0x2d, 0x49, 0x0c, 0xa7, 0x6c, 0xce,
	0xaa, 0x0e, 0x67, 0x9f, 0x0c, 0xb2, 0xa8, 0x68, 0x1b, 0xa3, 0x2d, 0xf1, 0x53, 0x48, 0x1f, 0x1d,
	0xba, 0xad, 0x8d, 0x8a, 0x91, 0x7c, 0xc7, 0x3d, 0x59, 0x8b, 0x6f, 0x14, 0xd5, 0x3a, 0xe4, 0x67,
	0x30, 0xf2, 0x96, 0xa1, 0x26, 0x4a, 0xfd, 0x06, 0x3d, 0xe3, 0x64, 0xbf, 0xf1, 0xb0, 0x67, 0x7c,
	0x79, 0x0d, 0xd9, 0x22, 0xfc, 0x2f, 0x77, 0x1b, 0xe5, 0x17, 0x90, 0x96, 0x2d, 0xa1, 0x23, 0x9e,
	0xe5, 0x7f, 0x77, 0x3a, 0x19, 0xe7, 0x3d, 0xff, 0x22, 0x79, 0x1e, 0x34, 0xcb, 0x65, 0x1a, 0x2e,
	0x70, 0xf5, 0x1d, 0x00, 0x00, 0xff, 0xff, 0x26, 0x02, 0xcf, 0xc9, 0x98, 0x01, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// ITokenCommandClient is the client API for ITokenCommand service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type ITokenCommandClient interface {
	Create(ctx context.Context, in *TokenRequest, opts ...grpc.CallOption) (*TokenResponse, error)
}

type iTokenCommandClient struct {
	cc grpc.ClientConnInterface
}

func NewITokenCommandClient(cc grpc.ClientConnInterface) ITokenCommandClient {
	return &iTokenCommandClient{cc}
}

func (c *iTokenCommandClient) Create(ctx context.Context, in *TokenRequest, opts ...grpc.CallOption) (*TokenResponse, error) {
	out := new(TokenResponse)
	err := c.cc.Invoke(ctx, "/ITokenCommand/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ITokenCommandServer is the server API for ITokenCommand service.
type ITokenCommandServer interface {
	Create(context.Context, *TokenRequest) (*TokenResponse, error)
}

// UnimplementedITokenCommandServer can be embedded to have forward compatible implementations.
type UnimplementedITokenCommandServer struct {
}

func (*UnimplementedITokenCommandServer) Create(ctx context.Context, req *TokenRequest) (*TokenResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}

func RegisterITokenCommandServer(s *grpc.Server, srv ITokenCommandServer) {
	s.RegisterService(&_ITokenCommand_serviceDesc, srv)
}

func _ITokenCommand_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(TokenRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ITokenCommandServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/ITokenCommand/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ITokenCommandServer).Create(ctx, req.(*TokenRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _ITokenCommand_serviceDesc = grpc.ServiceDesc{
	ServiceName: "ITokenCommand",
	HandlerType: (*ITokenCommandServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _ITokenCommand_Create_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "token_command.proto",
}
