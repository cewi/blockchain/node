package repository

import (
  "sync"

  "gitlab.com/cewi/blockchain/node/src/entities"
  "gitlab.com/cewi/blockchain/node/src/framework/database"
  "gitlab.com/cewi/blockchain/node/src/framework/repository"
)

type IBlockCommandRepository interface {
	Create(block *entities.Block) (string, error)
	Update(block *entities.Block) error
}

var blockRepository IBlockCommandRepository

type blockCommandRepository struct {
	repository.GenericCommandRepository[entities.Block]
}

func NewBlockCommandRepository() IBlockCommandRepository {
	lock := &sync.Mutex{}
	if blockRepository == nil {
		lock.Lock()
		defer lock.Unlock()
		blockRepository = &blockCommandRepository{
			repository.GenericCommandRepository[entities.Block]{
				MongoDataSource: repository_database.NewMongoDataSource(),
				DatabaseName:    "blockchain_manager",
				TableName:       "block",
			},
		}
	}
	return blockRepository
}

func (b *blockCommandRepository) Create(block *entities.Block) (string, error) {
	save, err := b.Save(block)
	if err != nil {
		return "", err
	}
	return save, nil
}

func (e *blockCommandRepository) Update(block *entities.Block) error {
	return e.GenericCommandRepository.Update(block.Hash, block)
}
