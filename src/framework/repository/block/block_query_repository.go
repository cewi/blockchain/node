package repository

import (
	"context"
	"sync"
	"time"

	. "github.com/gobeam/mongo-go-pagination"
	"gitlab.com/cewi/blockchain/node/src/entities"
	"gitlab.com/cewi/blockchain/node/src/framework/database"
	"gitlab.com/cewi/blockchain/node/src/framework/repository"
	"go.mongodb.org/mongo-driver/bson"
)

type IBlockQueryRepository interface {
	FindByCurrent(current bool) ([]*entities.Block, error)
	FindAllPage(page int, limit int, hashEpoch string) ([]*entities.Block, error)
}

var iBlockQueryRepository IBlockQueryRepository

type blockQueryRepository struct {
	repository.GenericQueryRepository[entities.Block]
}

func NewBlockQueryRepository() IBlockQueryRepository {
	lock := &sync.Mutex{}
	if iBlockQueryRepository == nil {
		lock.Lock()
		defer lock.Unlock()
		iBlockQueryRepository = &blockQueryRepository{
			repository.GenericQueryRepository[entities.Block]{
				MongoDataSource: repository_database.NewMongoDataSource(),
				DatabaseName:    "blockchain_manager",
				TableName:       "block",
			},
		}
	}
	return iBlockQueryRepository
}

func (b blockQueryRepository) FindByCurrent(current bool) ([]*entities.Block, error) {
	return b.FindAllByCustomized("current", current, "created_at", "desc")
}

func (b blockQueryRepository) FindAllPage(page int, limit int, hashEpoch string) ([]*entities.Block, error) {
	connect, _ := b.MongoDataSource.Connect()
	defer b.MongoDataSource.Close(connect)
	collection := b.MongoDataSource.DataSource(connect, b.DatabaseName, b.TableName)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var mappers []*entities.Block

	filters := bson.D{
		{Key: "$and",
			Value: bson.A{
				bson.D{{Key: "hash_epoch", Value: bson.D{{Key: "$eq", Value: hashEpoch}}}},
			},
		},
	}

	_, err := New(collection).Context(ctx).Limit(int64(limit)).Filter(filters).Sort("created_at", -1).Page(int64(page)).Decode(&mappers).Find()
	if err != nil {
		return nil, err
	}
	return mappers, nil
}
