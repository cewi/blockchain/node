package repository

import (
  "sync"

  "gitlab.com/cewi/blockchain/node/src/entities"
  repository_database "gitlab.com/cewi/blockchain/node/src/framework/database"
  "gitlab.com/cewi/blockchain/node/src/framework/repository"
)

type IBlockchainCommandRepository interface {
	Create(blockchain entities.Blockchain) (string, error)
	Update(blockchain *entities.Blockchain) error
}

type blockchainCommandRepository struct {
	repository.GenericCommandRepository[entities.Blockchain]
}

var iBlockchainRepository IBlockchainCommandRepository

func NewBlockchainCommandRepository() IBlockchainCommandRepository {
	lock := &sync.Mutex{}
	if iBlockchainRepository == nil {
		lock.Lock()
		defer lock.Unlock()

		iBlockchainRepository = &blockchainCommandRepository{
			repository.GenericCommandRepository[entities.Blockchain]{
				MongoDataSource: repository_database.NewMongoDataSource(),
				DatabaseName:    "blockchain_manager",
				TableName:       "blockchain",
			},
		}
	}
	return iBlockchainRepository
}

func (b *blockchainCommandRepository) Update(blockchain *entities.Blockchain) error {
	return b.GenericCommandRepository.Update(blockchain.Hash, blockchain)
}

func (b *blockchainCommandRepository) Create(blockchain entities.Blockchain) (string, error) {
	save, err := b.Save(&blockchain)
	if err != nil {
		return "", err
	}
	return save, nil
}
