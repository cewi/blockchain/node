package repository

import (
	"context"
	"sync"
	"time"

	"gitlab.com/cewi/blockchain/node/src/entities"
	repository_database "gitlab.com/cewi/blockchain/node/src/framework/database"
	pb "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
	"gitlab.com/cewi/blockchain/node/src/framework/repository"
	"go.mongodb.org/mongo-driver/bson"
)

type IBlockchainQueryRepository interface {
	FindBy(chainid string, hash string) (*entities.Blockchain, error)
	GetChainIdGenesis(req *pb.ChainIdRequestEmpty, chainIdGenesis string) (*pb.ChainIdResponse, error)
}

type blockchainQueryRepository struct {
	repository.GenericQueryRepository[entities.Blockchain]
}

var iBlockchainQueryRepository IBlockchainQueryRepository

func NewBlockchainQueryRepository() IBlockchainQueryRepository {
	lock := &sync.Mutex{}
	if iBlockchainQueryRepository == nil {
		lock.Lock()
		defer lock.Unlock()

		iBlockchainQueryRepository = &blockchainQueryRepository{
			repository.GenericQueryRepository[entities.Blockchain]{
				MongoDataSource: repository_database.NewMongoDataSource(),
				DatabaseName:    "blockchain_manager",
				TableName:       "blockchain",
			},
		}
	}
	return iBlockchainQueryRepository
}

func (b *blockchainQueryRepository) FindBy(chainid string, hash string) (*entities.Blockchain, error) {
	connect, _ := b.MongoDataSource.Connect()
	defer b.MongoDataSource.Close(connect)
	collection := b.MongoDataSource.DataSource(connect, b.DatabaseName, b.TableName)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var mapper *entities.Blockchain

	filters := bson.D{
		{Key: "$and",
			Value: bson.A{
				bson.D{{Key: "_id", Value: bson.D{{Key: "$eq", Value: hash}}}},
				bson.D{{Key: "chain_id", Value: bson.D{{Key: "$eq", Value: chainid}}}},
			},
		},
	}

	err := collection.FindOne(ctx, filters).Decode(&mapper)

	if err != nil {
		return nil, err
	}
	return mapper, nil

}

func (b *blockchainQueryRepository) GetChainIdGenesis(req *pb.ChainIdRequestEmpty, chainIdGenesis string) (*pb.ChainIdResponse, error) {
	connect, _ := b.MongoDataSource.Connect()
	defer b.MongoDataSource.Close(connect)
	collection := b.MongoDataSource.DataSource(connect, b.DatabaseName, b.TableName)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var mapper *entities.Blockchain

	filters := bson.D{
		{Key: "$and",
			Value: bson.A{
				bson.D{{Key: "chain_id", Value: bson.D{{Key: "$eq", Value: chainIdGenesis}}}},
			},
		},
	}

	err := collection.FindOne(ctx, filters).Decode(&mapper)

	if err != nil {
		return nil, err
	}

	return &pb.ChainIdResponse{
		ChainID:        mapper.ChainID,
		HashBlockchain: mapper.Hash,
	}, nil

}
