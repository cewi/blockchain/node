package repository

import (
  "sync"

  "gitlab.com/cewi/blockchain/node/src/entities"
  "gitlab.com/cewi/blockchain/node/src/framework/database"
  "gitlab.com/cewi/blockchain/node/src/framework/repository"
)

type IEpochCommandRepository interface {
	Create(epoch *entities.Epoch) (string, error)
	Update(epoch *entities.Epoch) error
}

var iEpochRepository IEpochCommandRepository

type epochCommandRepository struct {
	repository.GenericCommandRepository[entities.Epoch]
}

func NewEpochCommandRepository() IEpochCommandRepository {
	lock := &sync.Mutex{}
	if iEpochRepository == nil {
		lock.Lock()
		defer lock.Unlock()
		iEpochRepository = &epochCommandRepository{
			repository.GenericCommandRepository[entities.Epoch]{
				MongoDataSource: repository_database.NewMongoDataSource(),
				DatabaseName:    "blockchain_manager",
				TableName:       "epoch",
			},
		}
	}
	return iEpochRepository
}

func (e *epochCommandRepository) Update(epoch *entities.Epoch) error {
	return e.GenericCommandRepository.Update(epoch.Hash, epoch)
}

func (e *epochCommandRepository) Create(epoch *entities.Epoch) (string, error) {
	save, err := e.Save(epoch)
	if err != nil {
		return "", err
	}
	return save, nil
}
