package repository

import (
  "sync"

  "gitlab.com/cewi/blockchain/node/src/entities"
  "gitlab.com/cewi/blockchain/node/src/framework/database"
  "gitlab.com/cewi/blockchain/node/src/framework/repository"
)

type IEpochQueryRepository interface {
	FindByCurrent(current bool) ([]*entities.Epoch, error)
}

var iEpochQueryRepository IEpochQueryRepository

type epochQueryRepository struct {
	repository.GenericQueryRepository[entities.Epoch]
}

func NewEpochQueryRepository() IEpochQueryRepository {
	lock := &sync.Mutex{}
	if iEpochQueryRepository == nil {
		lock.Lock()
		defer lock.Unlock()
		iEpochQueryRepository = &epochQueryRepository{
			repository.GenericQueryRepository[entities.Epoch]{
				MongoDataSource: repository_database.NewMongoDataSource(),
				DatabaseName:    "blockchain_manager",
				TableName:       "epoch",
			},
		}
	}
	return iEpochQueryRepository
}

func (e *epochQueryRepository) FindByCurrent(current bool) ([]*entities.Epoch, error) {
	return e.FindAllByCustomized("current", current, "created_at", "desc")
}
