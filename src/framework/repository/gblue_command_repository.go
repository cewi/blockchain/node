package repository

import (
	"sync"

	"gitlab.com/cewi/blockchain/node/src/entities"
	"gitlab.com/cewi/blockchain/node/src/framework/database"
)

type ITblueCommandRepository interface {
	Create(tblue *entities.Tblue) (string, error)
}

var iTblueCommandRepository ITblueCommandRepository

type tblueCommandRepository struct {
	GenericCommandRepository[entities.Tblue]
}

func NewTblueCommandRepository() ITblueCommandRepository {
	lock := &sync.Mutex{}
	if iTblueCommandRepository == nil {
		lock.Lock()
		defer lock.Unlock()
		iTblueCommandRepository = &tblueCommandRepository{
			GenericCommandRepository[entities.Tblue]{
				MongoDataSource: repository_database.NewMongoDataSource(),
				DatabaseName:    "blockchain_manager",
				TableName:       "tblue",
			},
		}
	}
	return iTblueCommandRepository
}

func (e *tblueCommandRepository) Create(tblue *entities.Tblue) (string, error) {
	save, err := e.Save(tblue)
	if err != nil {
		return "", err
	}
	return save, nil
}
