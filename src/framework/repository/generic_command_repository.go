package repository

import (
	"context"
	"crypto/sha256"
	"fmt"
	"sort"
	"time"

	repository_database "gitlab.com/cewi/blockchain/node/src/framework/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type GenericCommandRepository[T any] struct {
	MongoDataSource repository_database.IMongoDataSource
	DatabaseName    string
	TableName       string
}

func (g GenericCommandRepository[T]) Save(mapper *T) (string, error) {
	connect, _ := g.MongoDataSource.Connect()
	defer g.MongoDataSource.Close(connect)
	collection := g.MongoDataSource.DataSource(connect, g.DatabaseName, g.TableName)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	response, err := collection.InsertOne(ctx, mapper)
	if err != nil {
		return "", err
	}
	g.RunCommand()
	return response.InsertedID.(string), nil
}

func (g GenericCommandRepository[T]) Update(id string, mapper *T) error {
	connect, _ := g.MongoDataSource.Connect()
	defer g.MongoDataSource.Close(connect)
	collection := g.MongoDataSource.DataSource(connect, g.DatabaseName, g.TableName)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": bson.M{"$eq": id}}
	_, err := collection.UpdateOne(ctx, filter, bson.M{"$set": mapper})
	if err != nil {
		return err
	}
	g.RunCommand()
	return nil

}

func (g GenericCommandRepository[T]) Delete(id string) error {
	connect, _ := g.MongoDataSource.Connect()
	defer g.MongoDataSource.Close(connect)
	collection := g.MongoDataSource.DataSource(connect, g.DatabaseName, g.TableName)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": id}
	_, err := collection.DeleteOne(ctx, filter)
	if err != nil {
		return err
	}
	g.RunCommand()
	return nil
}

func (g GenericCommandRepository[T]) RunCommand() {
	connect, _ := g.MongoDataSource.Connect()
	defer g.MongoDataSource.Close(connect)

	database := connect.Database(g.DatabaseName)
	command := bson.D{{"dbHash", 1}}

	var result bson.D

	err1 := database.RunCommand(context.TODO(), command).Decode(&result)
	if err1 != nil {
		fmt.Printf("Error: %v", err1.Error())
		return
	}

	m := result.Map()
	m["create_at"] = time.Now().UTC()
	m["hash"] = hashMapAndBool(true, m)

	collection := g.MongoDataSource.DataSource(connect, g.DatabaseName, "database_hash")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	_, err3 := collection.InsertOne(ctx, m)
	if err3 != nil {
		fmt.Printf("Error: %v", err1.Error())
		return
	}

}

func hashMapAndBool(b bool, m primitive.M) string {
	h := sha256.New()

	// hashing maps is not deterministic because it is not sorted, so we have
	// to manually hash each key value into another hash after sorting

	// first we hash the string representation of the bool
	h.Write([]byte(fmt.Sprintf("%v", b)))
	keys := make([]string, len(m))
	i := 0
	for k := range m {
		keys[i] = k
		i++
	}
	sort.Strings(keys)
	for _, k := range keys {
		v := m[k]
		// hash the key first then value before writing to the final sum
		// this is because if we just wrote the key and value as strings,
		// the following two maps would be equivalent:
		// { "k1" : "v1" } and { "k" : "1v1" }
		// so if we first hash the inputs, that ensures the final hash is
		// always unique unless there is a sha256 hash collision
		b := sha256.Sum256([]byte(fmt.Sprintf("%v", k)))
		h.Write(b[:])
		b = sha256.Sum256([]byte(fmt.Sprintf("%v", v)))
		h.Write(b[:])
	}

	return fmt.Sprintf("%x", h.Sum(nil))
}
