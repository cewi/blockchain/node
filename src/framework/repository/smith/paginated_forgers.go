package repository

import "gitlab.com/cewi/blockchain/node/src/entities"

type PaginatedSmiths struct {
	Smiths    []*entities.Smith
	Next      int64
	Page      int64
	Prev      int64
	Total     int64
	PerPage   int64
	TotalPage int64
}
