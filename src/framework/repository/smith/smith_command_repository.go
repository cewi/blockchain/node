package repository

import (
	"sync"

	"gitlab.com/cewi/blockchain/node/src/entities"
	"gitlab.com/cewi/blockchain/node/src/framework/database"
	"gitlab.com/cewi/blockchain/node/src/framework/repository"
)

type ISmithCommandRepository interface {
	Create(smith *entities.Smith) (string, error)
	Delete(smith *entities.Smith) error
	Update(smith *entities.Smith) error
}

var iSmithCommandRepository ISmithCommandRepository

type smithCommandRepository struct {
	repository.GenericCommandRepository[entities.Smith]
}

func NewSmithCommandRepository() ISmithCommandRepository {
	lock := &sync.Mutex{}
	if iSmithCommandRepository == nil {
		lock.Lock()
		defer lock.Unlock()
		iSmithCommandRepository = &smithCommandRepository{
			repository.GenericCommandRepository[entities.Smith]{
				MongoDataSource: repository_database.NewMongoDataSource(),
				DatabaseName:    "blockchain_manager",
				TableName:       "smith",
			},
		}
	}
	return iSmithCommandRepository
}

func (e *smithCommandRepository) Delete(smith *entities.Smith) error {
	return e.GenericCommandRepository.Delete(smith.Hash)
}

func (e *smithCommandRepository) Update(smith *entities.Smith) error {
	return e.GenericCommandRepository.Update(smith.Hash, smith)
}

func (e *smithCommandRepository) Create(smith *entities.Smith) (string, error) {
	save, err := e.Save(smith)
	if err != nil {
		return "", err
	}
	return save, nil
}
