package repository

import (
	"context"
	"encoding/json"
	"sync"
	"time"

	. "github.com/gobeam/mongo-go-pagination"
	"gitlab.com/cewi/blockchain/node/src/entities"
	"gitlab.com/cewi/blockchain/node/src/framework/database"
	pb "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
	"gitlab.com/cewi/blockchain/node/src/framework/repository"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type ISmithQueryRepository interface {
	FindBy(hash string, myAddress string) (*entities.Smith, error)
	FindAllPage(page int64, size int64, chainid string, hashBlockchain string) (*pb.SmithAllReponse, error)
	FindAll() ([]*entities.Smith, error)
	FindAllOrder() ([]*entities.Smith, error)
	FindByPubKey(base64 string) (*entities.Smith, error)
}

var iSmithQueryRepository ISmithQueryRepository

type smithQueryRepository struct {
	repository.GenericQueryRepository[entities.Smith]
}

func NewSmithQueryRepository() ISmithQueryRepository {
	lock := &sync.Mutex{}
	if iSmithQueryRepository == nil {
		lock.Lock()
		defer lock.Unlock()
		iSmithQueryRepository = &smithQueryRepository{
			repository.GenericQueryRepository[entities.Smith]{
				MongoDataSource: repository_database.NewMongoDataSource(),
				DatabaseName:    "blockchain_manager",
				TableName:       "smith",
			},
		}
	}
	return iSmithQueryRepository
}

func (f *smithQueryRepository) FindBy(hash string, myAddress string) (*entities.Smith, error) {
	connect, _ := f.MongoDataSource.Connect()
	defer f.MongoDataSource.Close(connect)
	collection := f.MongoDataSource.DataSource(connect, f.DatabaseName, f.TableName)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	var mapper *entities.Smith
	filters := bson.D{
		{Key: "$and",
			Value: bson.A{
				bson.D{{Key: "_id", Value: bson.D{{Key: "$eq", Value: hash}}}},
				bson.D{{Key: "my_address", Value: bson.D{{Key: "$eq", Value: myAddress}}}},
			},
		},
	}
	err := collection.FindOne(ctx, filters).Decode(&mapper)
	if err != nil {
		return nil, err
	}
	return mapper, nil
}

func (f *smithQueryRepository) FindByPubKey(pubKey string) (*entities.Smith, error) {
	connect, _ := f.MongoDataSource.Connect()
	defer f.MongoDataSource.Close(connect)
	collection := f.MongoDataSource.DataSource(connect, f.DatabaseName, f.TableName)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	var mapper *entities.Smith
	filters := bson.D{
		{Key: "$and",
			Value: bson.A{
				bson.D{{Key: "public_signature", Value: bson.D{{Key: "$eq", Value: pubKey}}}},
			},
		},
	}
	err := collection.FindOne(ctx, filters).Decode(&mapper)
	if err != nil {
		return nil, err
	}
	return mapper, nil
}

func (f *smithQueryRepository) FindAll() ([]*entities.Smith, error) {
	return f.GenericQueryRepository.FindAll()
}

func (f *smithQueryRepository) FindAllOrder() ([]*entities.Smith, error) {
	connect, _ := f.MongoDataSource.Connect()
	defer f.MongoDataSource.Close(connect)
	collection := f.MongoDataSource.DataSource(connect, f.DatabaseName, f.TableName)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var results []*entities.Smith

	opts := options.Find().SetSort(bson.D{{"number_of_smithd_blocks", -1}})

	cursor, err := collection.Find(ctx, bson.M{}, opts)

	if err != nil {
		return nil, err
	}
	if err = cursor.All(ctx, &results); err != nil {
		return nil, err
	}
	for _, result := range results {
		_, err := json.MarshalIndent(result, "", "    ")
		if err != nil {
			panic(err)
		}
	}
	return results, nil
}

func (f *smithQueryRepository) FindAllPage(page int64, size int64, chainid string, hashBlockchain string) (*pb.SmithAllReponse, error) {

	paginatedSmiths := &pb.SmithAllReponse{}

	connect, _ := f.MongoDataSource.Connect()
	defer f.MongoDataSource.Close(connect)
	collection := f.MongoDataSource.DataSource(connect, f.DatabaseName, f.TableName)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var smithss []*entities.Smith

	filters := bson.D{
		{Key: "$and",
			Value: bson.A{
				bson.D{{Key: "chain_id", Value: bson.D{{Key: "$eq", Value: chainid}}}},
				bson.D{{Key: "hash_blockchain", Value: bson.D{{Key: "$eq", Value: hashBlockchain}}}},
			},
		},
	}

	paginatedData, err := New(collection).Context(ctx).Limit(size).Filter(filters).Page(page).Decode(&smithss).Find()
	if err != nil {
		return paginatedSmiths, err
	}
	var smithssResponse []*pb.SmithResponse

	for _, smith := range smithss {
		smithssResponse = append(
			smithssResponse, &pb.SmithResponse{
				HashBlockchain: smith.HashBlockchain,
				ChainID:        smith.ChainID,
				Hash:           smith.Hash,
				MyAddress:      smith.MyAddress,
			},
		)
	}

	return &pb.SmithAllReponse{
		Smiths:    smithssResponse,
		Next:      paginatedData.Pagination.Next,
		Page:      paginatedData.Pagination.Page,
		Prev:      paginatedData.Pagination.Prev,
		Total:     paginatedData.Pagination.Total,
		PerPage:   paginatedData.Pagination.PerPage,
		TotalPage: paginatedData.Pagination.TotalPage,
	}, nil
}
