package repository

import (
	"sync"

	"gitlab.com/cewi/blockchain/node/src/entities"
	"gitlab.com/cewi/blockchain/node/src/framework/database"
	"gitlab.com/cewi/blockchain/node/src/framework/repository"
)

type IWinnerSmithCommandRepository interface {
	Create(winnersmith *entities.WinnerSmith) (string, error)
}

var iWinnerSmithCommand IWinnerSmithCommandRepository

type winnerSmithCommandRepository struct {
	repository.GenericCommandRepository[entities.WinnerSmith]
}

func NewWinnerSmithCommandRepository() IWinnerSmithCommandRepository {
	lock := &sync.Mutex{}
	if iWinnerSmithCommand == nil {
		lock.Lock()
		defer lock.Unlock()
		iWinnerSmithCommand = &winnerSmithCommandRepository{
			repository.GenericCommandRepository[entities.WinnerSmith]{
				MongoDataSource: repository_database.NewMongoDataSource(),
				DatabaseName:    "blockchain_manager",
				TableName:       "winner_smith",
			},
		}
	}
	return iWinnerSmithCommand
}

func (f *winnerSmithCommandRepository) Create(winnersmith *entities.WinnerSmith) (string, error) {
	return f.Save(winnersmith)
}
