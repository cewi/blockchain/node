package repository

import (
	"context"
	"encoding/json"
	"sync"
	"time"

	"gitlab.com/cewi/blockchain/node/src/entities"
	"gitlab.com/cewi/blockchain/node/src/framework/database"
	"gitlab.com/cewi/blockchain/node/src/framework/repository"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type IWinnerSmithQueryRepository interface {
	FindByWinner() (*entities.WinnerSmith, error)
}

var iWinnerQuerySmith IWinnerSmithQueryRepository

type winnerSmithRepository struct {
	repository.GenericQueryRepository[entities.WinnerSmith]
}

func NewWinnerSmithQueryRepository() IWinnerSmithQueryRepository {
	lock := &sync.Mutex{}
	if iWinnerQuerySmith == nil {
		lock.Lock()
		defer lock.Unlock()
		iWinnerQuerySmith = &winnerSmithRepository{
			repository.GenericQueryRepository[entities.WinnerSmith]{
				MongoDataSource: repository_database.NewMongoDataSource(),
				DatabaseName:    "blockchain_manager",
				TableName:       "winner_smith",
			},
		}
	}
	return iWinnerQuerySmith
}

func (f *winnerSmithRepository) FindByWinner() (*entities.WinnerSmith, error) {
	connect, _ := f.MongoDataSource.Connect()
	defer f.MongoDataSource.Close(connect)
	collection := f.MongoDataSource.DataSource(connect, f.DatabaseName, f.TableName)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var results []*entities.WinnerSmith
	sort := options.Find().SetLimit(1).SetSort(bson.D{{"created_at", -1}})

	values, err := collection.Find(ctx, bson.M{}, sort)

	if err != nil {
		return nil, err
	}
	if err = values.All(ctx, &results); err != nil {
		return nil, err
	}
	for _, result := range results {
		_, err := json.MarshalIndent(result, "", "    ")
		if err != nil {
			panic(err)
		}
	}
	if len(results) == 0 {
		return &entities.WinnerSmith{}, nil
	}
	return results[0], nil
}
