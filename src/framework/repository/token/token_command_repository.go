package repository

import (
	"sync"

	"gitlab.com/cewi/blockchain/node/src/entities"
	"gitlab.com/cewi/blockchain/node/src/framework/database"
	"gitlab.com/cewi/blockchain/node/src/framework/repository"
)

type ITokenCommnadRepository interface {
	Create(token *entities.Token) (string, error)
}

var iTokenCommnadRepository ITokenCommnadRepository

type tokenCommnadRepository struct {
	repository.GenericCommandRepository[entities.Token]
}

func NewTokenCommandRepository() ITokenCommnadRepository {
	lock := &sync.Mutex{}
	if iTokenCommnadRepository == nil {
		lock.Lock()
		defer lock.Unlock()
		iTokenCommnadRepository = &tokenCommnadRepository{
			repository.GenericCommandRepository[entities.Token]{
				MongoDataSource: repository_database.NewMongoDataSource(),
				DatabaseName:    "blockchain_manager",
				TableName:       "token",
			},
		}
	}
	return iTokenCommnadRepository
}

func (e *tokenCommnadRepository) Create(token *entities.Token) (string, error) {
	save, err := e.Save(token)
	if err != nil {
		return "", err
	}
	return save, nil
}
