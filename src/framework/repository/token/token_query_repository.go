package repository

import (
	"sync"

	"gitlab.com/cewi/blockchain/node/src/entities"
	repository_database "gitlab.com/cewi/blockchain/node/src/framework/database"
	pb "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
	"gitlab.com/cewi/blockchain/node/src/framework/repository"
)

type ITokenQueryRepository interface {
	FindByNickname(nickname string) (*entities.Token, error)
	FindByAll(req *pb.TokensRequest) (*pb.TokensResponse, error)
}

var iTokenQueryRepository ITokenQueryRepository

type tokenQueryReposittory struct {
	repository.GenericQueryRepository[entities.Token]
}

func NewTokenQueryRepository() ITokenQueryRepository {
	lock := &sync.Mutex{}
	if iTokenQueryRepository == nil {
		lock.Lock()
		defer lock.Unlock()
		iTokenQueryRepository = &tokenQueryReposittory{
			repository.GenericQueryRepository[entities.Token]{
				MongoDataSource: repository_database.NewMongoDataSource(),
				DatabaseName:    "blockchain_manager",
				TableName:       "token",
			},
		}
	}

	return iTokenQueryRepository
}

func (t tokenQueryReposittory) FindByNickname(nickname string) (*entities.Token, error) {
	return t.FindByColumnCustomized("nickname", nickname)
}

func (t tokenQueryReposittory) FindByAcronym(acronym string) (*entities.Token, error) {
	return t.FindByColumnCustomized("acronym", acronym)
}

func (t tokenQueryReposittory) FindByAll(req *pb.TokensRequest) (*pb.TokensResponse, error) {

	res := &pb.TokensResponse{
		Status: "FAIL",
	}

	data, tokens, err := t.FindAllPage(int(req.Page), int(req.Limit))

	if err != nil {
		return res, err
	}

	var tokensResponse []*pb.TokenResponse

	for _, token := range tokens {
		tokensResponse = append(
			tokensResponse, &pb.TokenResponse{
				HashToken: token.Hash,
				Acronym:   token.Acronym,
				Nickname:  token.Nickname,
			},
		)
	}

	return &pb.TokensResponse{
		Tokens:    tokensResponse,
		Next:      data.Pagination.Next,
		Page:      data.Pagination.Page,
		Prev:      data.Pagination.Prev,
		Total:     data.Pagination.Total,
		PerPage:   data.Pagination.PerPage,
		TotalPage: data.Pagination.TotalPage,
	}, nil

}
