package repository

import (
  "sync"

  "gitlab.com/cewi/blockchain/node/src/entities"
  "gitlab.com/cewi/blockchain/node/src/framework/database"
)

type ITransactionCommandRepository interface {
	Create(transaction *entities.Transaction) (string, error)
}

var iTransactionRepository ITransactionCommandRepository

type transactionCommandRepository struct {
	GenericCommandRepository[entities.Transaction]
}

func NewTransactionCommandRepository() ITransactionCommandRepository {
	lock := &sync.Mutex{}
	if iTransactionRepository == nil {
		lock.Lock()
		defer lock.Unlock()
		iTransactionRepository = &transactionCommandRepository{
			GenericCommandRepository[entities.Transaction]{
				MongoDataSource: repository_database.NewMongoDataSource(),
				DatabaseName:    "blockchain_manager",
				TableName:       "transaction",
			},
		}
	}
	return iTransactionRepository
}
func (e *transactionCommandRepository) Create(transaction *entities.Transaction) (string, error) {
	save, err := e.Save(transaction)
	if err != nil {
		return "", err
	}
	return save, nil
}
