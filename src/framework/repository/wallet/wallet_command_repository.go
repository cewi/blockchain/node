package repository

import (
	"sync"

	"gitlab.com/cewi/blockchain/node/src/entities"
	repository_database "gitlab.com/cewi/blockchain/node/src/framework/database"
	"gitlab.com/cewi/blockchain/node/src/framework/repository"
)

type IWalletCommandRepository interface {
	Create(wallet *entities.Wallet) (string, error)
}

var iWalletCommandRepository IWalletCommandRepository

type walletCommandRepository struct {
	repository.GenericCommandRepository[entities.Wallet]
}

func NewWalletCommandRepository() IWalletCommandRepository {

	lock := &sync.Mutex{}
	if iWalletCommandRepository == nil {
		lock.Lock()
		defer lock.Unlock()
		iWalletCommandRepository = &walletCommandRepository{
			repository.GenericCommandRepository[entities.Wallet]{
				MongoDataSource: repository_database.NewMongoDataSource(),
				DatabaseName:    "blockchain_manager",
				TableName:       "wallet",
			},
		}
	}

	return iWalletCommandRepository
}

func (w *walletCommandRepository) Create(wallet *entities.Wallet) (string, error) {
	return w.Save(wallet)
}
