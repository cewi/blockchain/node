package repository

import (
	"context"
	"sync"
	"time"

	"gitlab.com/cewi/blockchain/node/src/entities"
	repository_database "gitlab.com/cewi/blockchain/node/src/framework/database"
	"gitlab.com/cewi/blockchain/node/src/framework/repository"
	"go.mongodb.org/mongo-driver/bson"
)

type IWalletQueryRepository interface {
	FindByOne(hash string) (*entities.Wallet, error)
}

var iWalletQueryRepository IWalletQueryRepository

type walletQueryRepository struct {
	repository.GenericQueryRepository[entities.Wallet]
}

func NewWalletQueryRepository() IWalletQueryRepository {

	lock := &sync.Mutex{}
	if iWalletQueryRepository == nil {
		lock.Lock()
		defer lock.Unlock()
		iWalletQueryRepository = &walletQueryRepository{
			repository.GenericQueryRepository[entities.Wallet]{
				MongoDataSource: repository_database.NewMongoDataSource(),
				DatabaseName:    "blockchain_manager",
				TableName:       "wallet",
			},
		}
	}

	return iWalletQueryRepository
}

func (w *walletQueryRepository) FindByOne(hash string) (*entities.Wallet, error) {
	connect, _ := w.MongoDataSource.Connect()
	defer w.MongoDataSource.Close(connect)
	collection := w.MongoDataSource.DataSource(connect, w.DatabaseName, w.TableName)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	var mapper *entities.Wallet
	filters := bson.D{
		{Key: "$and",
			Value: bson.A{
				bson.D{{Key: "_id", Value: bson.D{{Key: "$eq", Value: hash}}}},
			},
		},
	}
	err := collection.FindOne(ctx, filters).Decode(&mapper)
	if err != nil {
		return nil, err
	}
	return mapper, nil
}
