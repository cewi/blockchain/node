package use_case

import (
	"errors"
	"log"
	"sync"
	"time"

	"gitlab.com/cewi/blockchain/node/src/entities"
	"gitlab.com/cewi/blockchain/node/src/framework/repository/block"
)

type IBlockInteractor interface {
	CreateBlockGenesis(hashEpoch string) (string, error)
	CreateNewBlock(epoch *entities.Epoch, smith *entities.Smith) (*entities.Block, *entities.Block, error)
	FindAllPage(page int, limit int, hashEpoch string) ([]*entities.Block, error)
	AddNewBlock(block *entities.Block) (string, error)
	Update(old *entities.Block) error
}

var iBlockInteractor IBlockInteractor

type blockInteractor struct {
	blockCommandRepository repository.IBlockCommandRepository
	blockQueryRepository   repository.IBlockQueryRepository
}

func NewBlockInteractor() IBlockInteractor {
	lock := &sync.Mutex{}
	if iBlockInteractor == nil {
		lock.Lock()
		defer lock.Unlock()
		iBlockInteractor = &blockInteractor{
			blockCommandRepository: repository.NewBlockCommandRepository(),
			blockQueryRepository:   repository.NewBlockQueryRepository(),
		}
	}
	return iBlockInteractor
}

func (b *blockInteractor) Update(block *entities.Block) error {
	return b.blockCommandRepository.Update(block)
}

func (b *blockInteractor) AddNewBlock(block *entities.Block) (string, error) {
	return b.blockCommandRepository.Create(block)
}

func (b *blockInteractor) CreateNewBlock(epoch *entities.Epoch, smith *entities.Smith) (*entities.Block, *entities.Block, error) {

	blocks, err := b.blockQueryRepository.FindByCurrent(true)
	if err != nil {
		log.Println(err)
		return nil, nil, err
	}

	if len(blocks) == 0 {
		return nil, nil, errors.New("block not found")
	}

	blockOld := blocks[0]
	blockOld.Current = false
	blockOld.ValidatorSmith = smith.Hash

	blockNew := &entities.Block{
		Smith:     smith.Hash,
		HashEpoch: epoch.Hash,
		Current:   true,
		CreatedAt: time.Now().UTC().String(),
		Nonce:     blockOld.Nonce + 1,
		PrevHash:  blockOld.Hash,
	}
	blockNew.GenerateHash()
	return blockNew, blockOld, nil
}

func (b *blockInteractor) CreateBlockGenesis(hashEpoch string) (string, error) {
	blockGenesis := &entities.Block{
		Current:   true,
		CreatedAt: time.Now().UTC().String(),
		Nonce:     1,
		HashEpoch: hashEpoch,
	}
	blockGenesis.GenerateHash()
	return b.blockCommandRepository.Create(blockGenesis)
}

func (b *blockInteractor) FindAllPage(page int, limit int, hashEpoch string) ([]*entities.Block, error) {
	return b.blockQueryRepository.FindAllPage(page, limit, hashEpoch)
}
