package use_case

import (
	"errors"
	"log"
	"math/rand"
	"strconv"
	"sync"
	"time"

	"gitlab.com/cewi/blockchain/node/src/entities"
	pb "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
	"gitlab.com/cewi/blockchain/node/src/framework/repository/blockchain"
	repositorysmith "gitlab.com/cewi/blockchain/node/src/framework/repository/smith"
	"gitlab.com/cewi/blockchain/node/src/utils"
)

const (
	ChainID          = "genesis_123"
	HashBlockchain   = "2e2836e83c5a7a82461af6d01e55c75abb02e6c29e63024c3031220d4d13d75c029fe9c771dcb0cba174c3877631eb467dc7a7bb4b3ed8f12ec414ef567b9821"
	PublicSignature  = "LS0tLS1CRUdJTiBSU0EgUFVCTElDIEtFWS0tLS0tCk1JSUJDZ0tDQVFFQTEwSnpXNzN0VzJXL29OT0tQVmQyaGVBdlNVOEdaNm1sbTBzbFY2MmUwZnZxaW4rdFZ5OUIKTW1sNG5SOTl4WlkxQ0EwTmdyY0hkTGcxdHhTYTMyVlJIb05WdGhYNDVXRXY3YUVmOXQ0ZEZ3UU5sVmFuRFc2eApzZVo5eVJBRVFscFBQZ2g3dG1WbEhwWnFlTjZtM1NiM2ZhVjNOcVdtNWJzNDQ4YTNhU2NqZmFGWm41b25tSjYyCkV1R2M2UnMwbHduUm5VZWVnY3hqaEwrcnFiZkJqNjNRSUZhdkhTV0o1ZThPeGc1VHA1UDNGczFnZkhtWDZFancKeHVhcytjWEhaYTVoWWgxSHBjS1RhR0tVTEtubDZuaE10cXZwdDJkUDFXS2hIRWpKTWNuQ0NkK3Y3Vm9mOW9teApHdzNod3JSVmJRYmZIZnZOUW45K0NXc3NsK1FBSDRjQ013SURBUUFCCi0tLS0tRU5EIFJTQSBQVUJMSUMgS0VZLS0tLS0K"
	PrivateSignature = "LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFb3dJQkFBS0NBUUVBMTBKelc3M3RXMlcvb05PS1BWZDJoZUF2U1U4R1o2bWxtMHNsVjYyZTBmdnFpbit0ClZ5OUJNbWw0blI5OXhaWTFDQTBOZ3JjSGRMZzF0eFNhMzJWUkhvTlZ0aFg0NVdFdjdhRWY5dDRkRndRTmxWYW4KRFc2eHNlWjl5UkFFUWxwUFBnaDd0bVZsSHBacWVONm0zU2IzZmFWM05xV201YnM0NDhhM2FTY2pmYUZabjVvbgptSjYyRXVHYzZSczBsd25SblVlZWdjeGpoTCtycWJmQmo2M1FJRmF2SFNXSjVlOE94ZzVUcDVQM0ZzMWdmSG1YCjZFand4dWFzK2NYSFphNWhZaDFIcGNLVGFHS1VMS25sNm5oTXRxdnB0MmRQMVdLaEhFakpNY25DQ2QrdjdWb2YKOW9teEd3M2h3clJWYlFiZkhmdk5RbjkrQ1dzc2wrUUFINGNDTXdJREFRQUJBb0lCQUNMOWhUenJjWkxDeGxTQgpMbDVzUWpMZXp6RHViSThiTDNVLzBORWd3c2xGVnNtdW1XR3JwOEJuRHBrVHptRDQycVAxWXpYVlJKdi9lWlBLCkFLYkJsRjhPWURkWitQeG9RTjh5cXJ4NVBHZEtYRjdqR0FLQjlOYURMZjBwNGxkYTBMaTNya3htelBvYzN5L0kKbVgwbllXR0dMUXV2LzZJbVBjZ2d2QjJXcEJrK2ZiNDc0V0dwb3J2Nk1wYmpDd3dHSjVmaHMxYk5qSEFVemljZApQZFlUVk1FK1NCV1hEMGJqaHd1ejh1RFpmVFV2SElTRXlmUFBsamdaVGRqejJ2TGVFdW9pWU11VFhnZE9DMkI1CmVKWWFFR01vbCtITjJrUUc5RzA1OU4xSTdOUHJ0LytOUENSc2JvRjNxbDNlbkt4RGNRMHYrUExMNlo0ejA1TUUKbDBwQ0hVa0NnWUVBNlBrLzNsaXFXZTkrd2ZMR013MXF5dFVKUjV1a2NpUXp4RU1xL2xLWDlvVkRxdWkveFlxRwpiQkQ5VmV3dXlnbjBFQ0FCWVhSOFlqWjg5QmZIWm5zeEhxMzF2OEM1d1hnMk42cUZBYnJ4TzRPSXhyNEVtSWFlCnJGK0FqK01jYlliQmU2ZGkzakdMNDlIa2tsVnVULzhMbDZBOEY4bVRYY3BnaTgveVYyNlloYjhDZ1lFQTdJai8KQUoyZ1NtbXRoNFo2UVZ5YlQ1ZzVFa2R2YytXeC9MR2MwUUFwekxZaHo3K2c0czRwRTJIUzU5aEZlbDEyNmowLwpDRG9LUkdtamhIMVAwa0I0amthSy92ZmxERGlCRTluUDVKcTh3NldrK1ZmaGpJTUF5b0xQaHV2RmVuWGpUb0lvClNYM1lzemI5Ynh4UWRpazZDa1VKSHFMNnhsZ2tuRmJObE5KQXFJMENnWUF0Q3h2VkluRjFSLy9MQWVSZU5JYmsKUm9JdnpTRGpUS3FSbEtiNFd3RzBrUmwvTHRiOUc0WlgySVVTbXNLRkg1MkhqSmlYdmd6YWRFeCthU2FOWXQ0aApYMkwzT3d6dklnM2hmMStOOFJYNCtycDBLb0w2OW5ReFFabTZ1QjlqOGJ6dzZuRU1JcW80VnZtSmJXa1JvRzhlClZzV25sN3FkVkthTUFaMktXQUMzUXdLQmdRRGZFcWhqZlVUQlNQN2wxSGFTRS9OSlVvZG9iTVI0cWdSOUMwaVkKZHkzMHFVUWorRmFUNzlnZk5sL0FWVi9ZOGpLRzRMSUVZVEJ6cXFrUzkxZURNMWcrM2RxU2NWbXJkOUtMMVJ4UwpQWGlPanphU0RVWml6TjIvSHpUMnluNVlhcnV2dStLZ3hMWE05cXgyMEY0MXdkWEIvNitGSERHalExZHJ0aFYyCkovL2d0UUtCZ0U3K3B0dVpQTllQVnVGY0hnaldDRC8zaHZZSVZlVVBXTC9kUHhvM3gyNTRIbGVyeUFzTE9YTEwKN2x5cW1OWVBUMTZCSHV4WUQ4REVFc0NocWJ1ZkdIY2E0OTZIVHJaejc1NmdOd1NRUjNzcURSMDZHRlA5VlV2SQpBU3haWmFKLzk1VzNFbUUvbU9QeE82ZTRtcFdUdUJCNWtVUXYvVlFLSFNwd2orbjE5NFFMCi0tLS0tRU5EIFJTQSBQUklWQVRFIEtFWS0tLS0tCg"
)

type IBlockchainInteractor interface {
	CreateBlockchainGenesis() error
	IsExist(chainID string, hashBlockchain string) (bool, error)
	UpdateRankSmiths()
	CreateNewBlock(req *pb.BlockchainRequest) (*pb.BlockchainResponse, error)
}

type blockchainInteractor struct {
	blockchainCommandRepository  repository.IBlockchainCommandRepository
	blockchainQueryRepository    repository.IBlockchainQueryRepository
	signatureInteractor          ISignatureInteractor
	epochInteractor              IEpochInteractor
	blockInteractor              IBlockInteractor
	transactionInteractor        ITransactionInteractor
	tokenInteractor              ITokenInteractor
	smithQueryRepository         repositorysmith.ISmithQueryRepository
	winnerSmithCommandRepository repositorysmith.IWinnerSmithCommandRepository
}

var iBlockchainInteractor IBlockchainInteractor

func NewBlockchainInteractor() IBlockchainInteractor {
	lock := &sync.Mutex{}
	if iBlockchainInteractor == nil {
		lock.Lock()
		defer lock.Unlock()
		interactor := blockchainInteractor{
			blockchainCommandRepository:  repository.NewBlockchainCommandRepository(),
			blockchainQueryRepository:    repository.NewBlockchainQueryRepository(),
			epochInteractor:              NewEpochInteractor(),
			blockInteractor:              NewBlockInteractor(),
			transactionInteractor:        NewTransactionInteractor(),
			signatureInteractor:          NewSignatureInteractor(),
			tokenInteractor:              NewTokenInteractor(),
			smithQueryRepository:         repositorysmith.NewSmithQueryRepository(),
			winnerSmithCommandRepository: repositorysmith.NewWinnerSmithCommandRepository(),
		}
		iBlockchainInteractor = &interactor
		utils.RunCronJobs("0 0 */5 * *", CreateNewEpoch(interactor))
		utils.RunCronJobs("*/1 * * * *", DrawSmith(interactor))
	}
	return iBlockchainInteractor
}

// somente a blockchain genesis cria novas épocas
func CreateNewEpoch(interactor blockchainInteractor) func() {
	return func() {
		exist, err := interactor.IsExist(ChainID, HashBlockchain)
		if err != nil {
			log.Println(err)
			return
		}
		if exist {
			interactor.epochInteractor.CreateNewEpoch()
		}
	}
}

// TODO somente a blockchain genesis pode sortear o forjador
// TODO Refatorar esse método
func DrawSmith(interactor blockchainInteractor) func() {
	return func() {
		rand.Seed(time.Now().UnixNano())
		// CreateEpochGenesis a slice of smiths with a positive stake
		var winnerPool []*entities.Smith
		var totalStake uint64 = 0

		smiths, err1 := interactor.smithQueryRepository.FindAll()

		if err1 != nil {
			log.Println(err1)
			return
		}

		for _, smith := range smiths {
			balance, err3 := getBalance(PrivateSignature, smith)
			if err3 != nil {
				log.Println(err3)
				return
			}
			if balance > 0 {
				winnerPool = append(winnerPool, smith)
				totalStake += balance
			}
		}
		// Return an error if there are no smiths with a positive stake
		if winnerPool == nil {
			log.Println(errors.New("there are no smith  with stake in the network"))
			return
		}
		// Shuffle the slice of smiths and choose a random number between 0 and the total stake
		rand.Shuffle(
			len(winnerPool), func(i, j int) {
				winnerPool[i], winnerPool[j] = winnerPool[j], winnerPool[i]
			},
		)
		winnerNumber := rand.Intn(int(totalStake))
		// Return the first smith  in the shuffled slice with a stake greater than or equal to the chosen number
		var winnerSun uint64 = 0
		for _, smith := range smiths {
			balance, err4 := getBalance(PrivateSignature, smith)
			if err4 != nil {
				log.Println(err4)
				return
			}

			winnerSun += balance
			if winnerNumber < int(winnerSun) {
				//interactor.createBlock(smith) FIXME esse método não pode ser chamado aqui mais
				now := time.Now().UTC().String()
				generate, _ := utils.NewHashUtils().Generate(now)
				_, errFor := interactor.winnerSmithCommandRepository.Create(
					&entities.WinnerSmith{
						ID:              generate,
						Hash:            smith.Hash,
						CreatedAt:       now,
						PublicSignature: smith.PublicSignature,
					},
				)
				if errFor != nil {
					log.Println(err4)
					return
				}
				return
			}
		}
		// Return an error if no smith  was chosen
		log.Println("selected number is greater than total stake")

	}
}

func (b *blockchainInteractor) CreateNewBlock(req *pb.BlockchainRequest) (*pb.BlockchainResponse, error) {

	res := &pb.BlockchainResponse{
		Status: "FAIL",
	}

	signatureUtil := utils.NewSignatureUtil()

	blockJson, err1 := signatureUtil.Base64ToString(req.Blocks)
	if err1 != nil {
		res.Message = err1.Error()
		return res, err1
	}

	signatureString, err2 := signatureUtil.Base64ToString(req.Signature)
	if err2 != nil {
		res.Message = err2.Error()
		return res, err2
	}

	publicSignatureString, err3 := signatureUtil.Base64ToString(PublicSignature)
	if err3 != nil {
		res.Message = err3.Error()
		return res, err3
	}

	publicSignature, err4 := signatureUtil.StringToPublicKey(publicSignatureString)
	if err4 != nil {
		res.Message = err4.Error()
		return res, err4
	}

	pss := signatureUtil.VerifyPSS(publicSignature, blockJson, []byte(signatureString))
	if pss {
		smith, err5 := b.smithQueryRepository.FindBy(req.HashSmith, req.MyAddress)
		if err5 != nil {
			res.Message = err5.Error()
			return res, err5
		}

		blockchain, err6 := b.blockchainQueryRepository.FindBy(ChainID, HashBlockchain)
		if err6 != nil {
			res.Message = err6.Error()
			return res, err6
		}

		err7 := b.epochInteractor.NewBlock(blockchain, smith)
		if err7 != nil {
			res.Message = err7.Error()
			return res, err7
		}

		b.UpdateRankSmiths()

		res.Message = "create block success"
		res.Status = "OK"
		return res, nil
	}

	res.Message = "Signature invalid"
	return res, errors.New(res.Message)

}

func (b *blockchainInteractor) IsExist(chainID string, hashBlockchain string) (bool, error) {
	blockchain, err := b.blockchainQueryRepository.FindBy(chainID, hashBlockchain)
	if err != nil {
		return false, err
	}
	return blockchain != nil, nil
}

func (b *blockchainInteractor) CreateBlockchainGenesis() error {
	// TODO buscar na rede genesis a blockchain genesis, se existe não poderá ser criada a blockchain.
	// TODO se não houver criar e registrar no banco de dados e iniciar como um genesis;
	if false {
		blockchain, err := entities.LoadBlockchainGenesis()
		if err != nil {
			return err
		}
		blockchain.Hash = HashBlockchain
		blockchain.ChainID = ChainID

		_, err = b.blockchainCommandRepository.Create(*blockchain)
		if err != nil {
			return err
		}

		_, errToken := b.tokenInteractor.CreateTokenGenesis(blockchain.ChainID, blockchain.Hash)
		if errToken != nil {
			return errToken
		}

		hashEpoch, errEpoch := b.epochInteractor.CreateEpochGenesis(blockchain.ChainID, blockchain.Hash)
		if errEpoch != nil {
			return errEpoch
		}

		hashBlock, errBlock := b.blockInteractor.CreateBlockGenesis(hashEpoch)
		if errBlock != nil {
			return errBlock
		}

		_, errTrans := b.transactionInteractor.CreateTrasactionGenesis(hashEpoch, hashBlock)
		if errTrans != nil {
			return errTrans
		}

	}
	return nil
}

func (b *blockchainInteractor) UpdateRankSmiths() {
	smiths, err := b.smithQueryRepository.FindAllOrder()
	if err != nil {
		log.Println(err)
		return
	}
	blockchain, errbloc := b.blockchainQueryRepository.FindBy(ChainID, HashBlockchain)

	if errbloc != nil {
		log.Println(errbloc)
		return
	}
	blockchain.RankOfSmith = smiths
	errBloclchain := b.blockchainCommandRepository.Update(blockchain)
	if errBloclchain != nil {
		log.Println(errBloclchain)
		return
	}
}

func getBalance(privateSignature string, smith *entities.Smith) (uint64, error) {
	signatureUtil := utils.NewSignatureUtil()
	keyPem, err1 := signatureUtil.Base64ToString(privateSignature)
	if err1 != nil {
		log.Println(err1)
		return 0, err1
	}

	privateKey, err2 := signatureUtil.StringToPrimaryKey(keyPem)
	if err2 != nil {
		log.Println(err2)
		return 0, err2
	}

	balanceString, err4 := signatureUtil.Base64ToString(smith.Wallet.Balance)
	if err4 != nil {
		log.Println(err4)
		return 0, err4
	}

	dencryptBalance, err4 := signatureUtil.Dencrypt(privateKey, []byte(balanceString))
	if err4 != nil {
		log.Println(err4)
		return 0, err4
	}
	balance, _ := strconv.ParseUint(dencryptBalance, 10, 64)
	return balance, nil
}
