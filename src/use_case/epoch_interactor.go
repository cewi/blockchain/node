package use_case

import (
	"errors"
	"log"
	"sync"
	"time"

	"gitlab.com/cewi/blockchain/node/src/entities"
	repository3 "gitlab.com/cewi/blockchain/node/src/framework/repository/blockchain"
	"gitlab.com/cewi/blockchain/node/src/framework/repository/epoch"
	repository2 "gitlab.com/cewi/blockchain/node/src/framework/repository/smith"
)

type IEpochInteractor interface {
	CreateEpochGenesis(chainIdBlock string, hashBlockchain string) (string, error)
	CreateNewEpoch()
	LastEpoch() (*entities.Epoch, error)
	NewBlock(blockchain *entities.Blockchain, smith *entities.Smith) error
}

var iEpochInteractor IEpochInteractor

type epochInteractor struct {
	epochCommandRepository       repository.IEpochCommandRepository
	epochQueryRepository         repository.IEpochQueryRepository
	blockInteractor              IBlockInteractor
	smithCommandRepository       repository2.ISmithCommandRepository
	winnerSmithCommandRepository repository2.IWinnerSmithCommandRepository
	smithQueryRepository         repository2.ISmithQueryRepository
	blockchainQueryRepository    repository3.IBlockchainQueryRepository
	iSmithInteractor             ISmithInteractor
}

func NewEpochInteractor() IEpochInteractor {
	lock := &sync.Mutex{}
	if iEpochInteractor == nil {
		lock.Lock()
		defer lock.Unlock()
		iEpochInteractor = &epochInteractor{
			epochCommandRepository:       repository.NewEpochCommandRepository(),
			epochQueryRepository:         repository.NewEpochQueryRepository(),
			blockInteractor:              NewBlockInteractor(),
			smithCommandRepository:       repository2.NewSmithCommandRepository(),
			winnerSmithCommandRepository: repository2.NewWinnerSmithCommandRepository(),
			smithQueryRepository:         repository2.NewSmithQueryRepository(),
			blockchainQueryRepository:    repository3.NewBlockchainQueryRepository(),
			iSmithInteractor:             NewSmithInteractor(),
		}
	}
	return iEpochInteractor
}

func (e epochInteractor) CreateEpochGenesis(chainIdBlock string, hashBlockchain string) (string, error) {
	epoch, err := entities.LoadEpochGenesis()
	if err != nil {
		return "", err
	}
	epoch.ChainID = chainIdBlock
	epoch.HashBlockchain = hashBlockchain
	epoch.GenerateHash()
	return e.epochCommandRepository.Create(epoch)
}

func (e *epochInteractor) LastEpoch() (*entities.Epoch, error) {
	epoches, err := e.epochQueryRepository.FindByCurrent(true)
	if err != nil {
		return nil, err
	}
	if len(epoches) == 0 {
		return nil, nil
	}
	return epoches[0], err
}

// Somente a blockchain pode criar uma época
func (e *epochInteractor) CreateNewEpoch() {
	epoches, err := e.epochQueryRepository.FindByCurrent(true)
	if err != nil {
		log.Println(err)
		return
	}

	oldEpoch := epoches[0]
	oldEpoch.Current = false

	newEpoch := &entities.Epoch{
		ChainID:        oldEpoch.ChainID,
		HashBlockchain: oldEpoch.HashBlockchain,
		CreatedAt:      time.Now().UTC().String(),
		Nonce:          oldEpoch.Nonce + 1,
		PrevHash:       oldEpoch.Hash,
		Current:        true,
	}
	newEpoch.GenerateHash()

	errUp := e.epochCommandRepository.Update(oldEpoch)
	if errUp != nil {
		log.Println(errUp)
		return
	}
	_, errCr := e.epochCommandRepository.Create(newEpoch)
	if errCr != nil {
		log.Println(errCr)
		return
	}

}

func (e *epochInteractor) NewBlock(blockchain *entities.Blockchain, smith *entities.Smith) error {
	if smith == nil {
		return errors.New("invalid smith parameter")
	}
	if blockchain == nil {
		return errors.New("invalid blockchainGenesis parameter")
	}

	epochsCurrent, err1 := e.epochQueryRepository.FindByCurrent(true)
	if err1 != nil {
		return err1
	}

	if len(epochsCurrent) == 0 {
		return errors.New("epoch not found")
	}

	newBlock, blockOld, errBlock := e.blockInteractor.CreateNewBlock(epochsCurrent[0], smith)
	if errBlock != nil {
		return errBlock
	}

	_, err10 := e.blockInteractor.AddNewBlock(newBlock)
	if err10 != nil {
		return err10
	}

	errUp := e.blockInteractor.Update(blockOld)
	if errUp != nil {
		log.Println(errUp)
		return errUp
	}

	balance, err2 := getBalance(PrivateSignature, smith)
	if err2 != nil {
		return err2
	}

	balance = balance + blockchain.Reward

	smith.NumberOfSmithBlocks += 1

	err7 := e.iSmithInteractor.UpdateBalanceSmith(balance, smith)

	if err7 != nil {
		return err7
	}

	return nil
}
