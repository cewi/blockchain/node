package use_case

import (
	"sync"
	"time"

	"gitlab.com/cewi/blockchain/node/src/entities"
	"gitlab.com/cewi/blockchain/node/src/framework/repository"
	"gitlab.com/cewi/blockchain/node/src/utils"
)

const (
	serialGenesis = "0000000000000000000000000"
)

type ITblueInteractor interface {
	Create(hashToken string, chainId string, hashBlockchain string, balance string) (string, error)
}

var iTblueInteractor ITblueInteractor

type tblueInteractor struct {
	tblueRepository repository.ITblueCommandRepository
}

func NewTblueInteractor() ITblueInteractor {
	lock := &sync.Mutex{}
	if iTblueInteractor == nil {
		lock.Lock()
		defer lock.Unlock()
		iTblueInteractor = &tblueInteractor{
			tblueRepository: repository.NewTblueCommandRepository(),
		}
	}
	return iTblueInteractor
}

func (g *tblueInteractor) Create(hashToken string, chainId string, hashBlockchain string, balance string) (string, error) {
	tblue := &entities.Tblue{
		CreatedAt:      time.Now().UTC().String(),
		HashToken:      hashToken,
		ChainID:        chainId,
		HashBlockchain: hashBlockchain,
		Serial:         serialGenesis,
		Address:        utils.NewAddressUtils().RandAddress(),
		Balance:        balance,
	}

	tblue.GenerateHash()
	return g.tblueRepository.Create(tblue)
}
