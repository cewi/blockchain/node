package use_case

import (
  crand "crypto/rand"
  "crypto/rsa"
  "crypto/x509"
  "encoding/pem"
  pb "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
  "gitlab.com/cewi/blockchain/node/src/utils"
  "sync"
)

type ISignatureInteractor interface {
  GenerateSignature() (*pb.SignatureResponse, error)
}

type signatureInteractor struct {
  signatureUtil *utils.SignatureUtil
}

var iSignatureInteractor ISignatureInteractor

func NewSignatureInteractor() ISignatureInteractor {
  lock := &sync.Mutex{}
  if iSignatureInteractor == nil {
    defer lock.Unlock()
    lock.Lock()
    iSignatureInteractor = &signatureInteractor{
      signatureUtil: utils.NewSignatureUtil(),
    }
  }
  return iSignatureInteractor
}

func (s signatureInteractor) GenerateSignature() (*pb.SignatureResponse, error) {
  privateKey, err := rsa.GenerateKey(crand.Reader, 2048)
  if err != nil {
    return &pb.SignatureResponse{Message: err.Error(), Status: "FAILED"}, err
  }
  keyPEM := pem.EncodeToMemory(
    &pem.Block{
      Type:  "RSA PRIVATE KEY",
      Bytes: x509.MarshalPKCS1PrivateKey(privateKey),
    },
  )

  publicKey := privateKey.Public()
  pubPEM := pem.EncodeToMemory(
    &pem.Block{
      Type:  "RSA PUBLIC KEY",
      Bytes: x509.MarshalPKCS1PublicKey(publicKey.(*rsa.PublicKey)),
    },
  )
  keyPEMString := s.signatureUtil.ByteToString(keyPEM)
  pubPEMString := s.signatureUtil.ByteToString(pubPEM)
  return &pb.SignatureResponse{
    Status:           "OK",
    PrivateKey:       keyPEMString,
    PublicKey:        pubPEMString,
    PrivateKeyBase64: s.signatureUtil.StringToBase64(keyPEMString),
    PublicKeyBase64:  s.signatureUtil.StringToBase64(pubPEMString),
  }, nil

}
