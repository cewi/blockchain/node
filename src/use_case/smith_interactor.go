package use_case

import (
	"encoding/json"
	"errors"
	"log"
	"strconv"
	"sync"
	"time"

	"gitlab.com/cewi/blockchain/node/src/entities"
	pb "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
	repository2 "gitlab.com/cewi/blockchain/node/src/framework/repository/block"
	repository_blockchain "gitlab.com/cewi/blockchain/node/src/framework/repository/blockchain"
	repository3 "gitlab.com/cewi/blockchain/node/src/framework/repository/epoch"
	repository "gitlab.com/cewi/blockchain/node/src/framework/repository/smith"
	"gitlab.com/cewi/blockchain/node/src/utils"
)

type ISmithInteractor interface {
	RegisterSmith(req *pb.SmithRequest) (*pb.SmithResponseCommand, error)
	ToStopBeingSmith(req *pb.SmithRemoveRequest) (*pb.SmithResponseCommand, error)
	CheckWinner(hash string) (*pb.SmithCheckResponse, error)
	SendFine(req *pb.SmithFineRequest) (*pb.SmithResponseCommand, error)
	GetBalance(req *pb.SmithCheckRequest) (*pb.SmithBalanceResponse, error)
	UpdateBalanceSmith(balance uint64, smith *entities.Smith) error
}

var iSmithInteractor ISmithInteractor

type smithInteractor struct {
	smithCommandRepository     repository.ISmithCommandRepository
	smithQueryRepository       repository.ISmithQueryRepository
	signatureInteractor        ISignatureInteractor
	blockchainQueryRepository  repository_blockchain.IBlockchainQueryRepository
	winnerSmithQueryRepository repository.IWinnerSmithQueryRepository
	blockQueryRepository       repository2.IBlockQueryRepository
	epochQueryRepository       repository3.IEpochQueryRepository
}

func NewSmithInteractor() ISmithInteractor {
	lock := &sync.Mutex{}
	if iSmithInteractor == nil {
		lock.Lock()
		defer lock.Unlock()
		iSmithInteractor = &smithInteractor{
			smithCommandRepository:     repository.NewSmithCommandRepository(),
			smithQueryRepository:       repository.NewSmithQueryRepository(),
			signatureInteractor:        NewSignatureInteractor(),
			blockchainQueryRepository:  repository_blockchain.NewBlockchainQueryRepository(),
			winnerSmithQueryRepository: repository.NewWinnerSmithQueryRepository(),
			blockQueryRepository:       repository2.NewBlockQueryRepository(),
			epochQueryRepository:       repository3.NewEpochQueryRepository(),
		}
	}
	return iSmithInteractor
}

func (f *smithInteractor) ToStopBeingSmith(req *pb.SmithRemoveRequest) (*pb.SmithResponseCommand, error) {
	resp := &pb.SmithResponseCommand{
		Status: "FAILED",
	}

	smith, err := f.smithQueryRepository.FindBy(req.Hash, req.MyAddress)

	if err != nil {
		resp.Message = err.Error()
		return resp, err
	}

	errForg := f.smithCommandRepository.Delete(smith)
	if errForg != nil {
		resp.Message = errForg.Error()
		return resp, errForg
	}
	resp.Status = "OK"
	resp.Message = "smith has been successfully removed"
	return resp, nil
}

func (f *smithInteractor) RegisterSmith(req *pb.SmithRequest) (*pb.SmithResponseCommand, error) {
	res := &pb.SmithResponseCommand{
		Status: "FAILED",
	}

	if req.PublicSignatureBase64 == "" {
		res.Message = "public signature not found"
		return res, errors.New(res.Message)
	}

	if req.MyAddress == "" {
		res.Message = "address smith not found"
		return res, errors.New(res.Message)
	}

	if req.ChainID == "" {
		res.Message = "chain id from blockchain not found"
		return res, errors.New(res.Message)
	}

	if req.HashBlockchain == "" {
		res.Message = "hash blockchain not found"
		return res, errors.New(res.Message)
	}

	blockchain, errBl2 := f.blockchainQueryRepository.FindBy(ChainID, HashBlockchain)
	if errBl2 != nil {
		res.Message = errBl2.Error()
		return res, errBl2
	}

	if blockchain == nil {
		res.Message = "blockchain not found"
		return res, errors.New(res.Message)
	}

	smithExist, _ := f.smithQueryRepository.FindByPubKey(req.PublicSignatureBase64)

	if smithExist != nil {
		res.Message = "smith already exists"
		return res, errors.New(res.Message)
	}

	signatureUtil := utils.NewSignatureUtil()
	pub, err1 := signatureUtil.Base64ToString(PublicSignature)
	if err1 != nil {
		res.Message = err1.Error()
		return res, err1
	}

	publicKey, err3 := signatureUtil.StringToPublicKey(pub)
	if err3 != nil {
		res.Message = err3.Error()
		return res, err3
	}

	address := utils.NewAddressUtils().RandAddress()
	encryptAddress, err4 := signatureUtil.Encrypt(publicKey, address)
	if err4 != nil {
		res.Message = err4.Error()
		return res, err4
	}

	encryptMyAddress, err6 := signatureUtil.Encrypt(publicKey, req.MyAddress)
	if err6 != nil {
		res.Message = err6.Error()
		return res, err6
	}

	encryptBalance, err5 := signatureUtil.Encrypt(publicKey, strconv.Itoa(0))
	if err5 != nil {
		res.Message = err5.Error()
		return res, err5
	}

	smith := &entities.Smith{
		CreatedAt:           time.Now().UTC().String(),
		ChainID:             req.ChainID,
		MyAddress:           signatureUtil.StringToBase64(signatureUtil.ByteToString(encryptMyAddress)),
		HashBlockchain:      req.HashBlockchain,
		PublicSignature:     req.PublicSignatureBase64,
		NumberOfSmithBlocks: 0,
		Wallet: &entities.Wallet{
			Address: signatureUtil.StringToBase64(string(encryptAddress)),
			Balance: signatureUtil.StringToBase64(string(encryptBalance)),
		},
	}
	smith.GenerateHash()
	hashSmith, err := f.smithCommandRepository.Create(smith)

	if err != nil {
		res.Message = err.Error()
		return res, errors.New(res.Message)
	}

	res.Status = "OK"
	res.Message = "smith registered success"
	res.BettingAddress = smith.Wallet.Address
	res.Hash = hashSmith
	res.MyAddress = smith.MyAddress
	return res, nil
}

func (f *smithInteractor) CheckWinner(hash string) (*pb.SmithCheckResponse, error) {

	res := &pb.SmithCheckResponse{
		Status: "FAILED",
	}

	smith, err := f.winnerSmithQueryRepository.FindByWinner()
	if err != nil {
		res.Status = "FAILED"
		res.Message = err.Error()
		return res, err
	}

	if smith == nil {
		res.Status = "FAILED"
		res.Message = "winner smith not found"
		return res, nil
	}

	if smith.Hash == hash {

		epochCurrent, errEpo := f.epochQueryRepository.FindByCurrent(true)
		if errEpo != nil || len(epochCurrent) == 0 {
			res.Status = "FAILED"
			res.Message = errEpo.Error()
			return res, errEpo
		}

		blocks, errBlock := f.blockQueryRepository.FindAllPage(0, 3, epochCurrent[0].Hash)

		if errBlock != nil || len(blocks) == 0 {
			res.Status = "FAILED"
			res.Message = errBlock.Error()
			return res, errBlock
		}

		err2 := f.signWithBlockchain(res, blocks)
		if err2 != nil {
			res.Status = "FAILED"
			res.Message = err2.Error()
			return res, err2
		}

		errBase := f.blockToBase64(smith, blocks, res)
		if errBase != nil {
			res.Status = "FAILED"
			res.Message = errBase.Error()
			return res, errBase
		}

		errEpochBase := f.epochToBase64(smith, epochCurrent[0], res)
		if errEpochBase != nil {
			res.Status = "FAILED"
			res.Message = errEpochBase.Error()
			return res, errEpochBase
		}

		res.Status = "OK"
		res.YouWon = true
		return res, nil
	}

	res.Status = "OK"
	res.YouWon = false
	return res, nil
}

func (f *smithInteractor) SendFine(req *pb.SmithFineRequest) (*pb.SmithResponseCommand, error) {
	res := &pb.SmithResponseCommand{
		Status: "FAILED",
	}

	blockchain, errBlockchain := f.blockchainQueryRepository.FindBy(ChainID, HashBlockchain)
	if errBlockchain != nil {
		res.Message = errBlockchain.Error()
		return res, errBlockchain
	}

	smith, err := f.smithQueryRepository.FindBy(req.Hash, req.MyAddress)
	if err != nil {
		res.Message = err.Error()
		return res, err
	}

	balance, errBalance := f.getBalance(PrivateSignature, smith)
	if errBalance != nil {
		res.Message = errBalance.Error()
		return res, errBalance
	}

	balance -= blockchain.Fine

	errUpdate := f.UpdateBalanceSmith(balance, smith)
	if errUpdate != nil {
		res.Message = errUpdate.Error()
		return res, errUpdate
	}

	res.Status = "OK"

	return res, nil
}

func (f *smithInteractor) GetBalance(req *pb.SmithCheckRequest) (*pb.SmithBalanceResponse, error) {
	res := &pb.SmithBalanceResponse{
		Status: "FAIL",
	}
	smith, err1 := f.smithQueryRepository.FindBy(req.Hash, req.MyAddress)
	if err1 != nil {
		res.Message = err1.Error()
		return res, err1
	}

	balanceBase64 := smith.Wallet.Balance

	signatureUtil := utils.NewSignatureUtil()

	privateSignatureString, err2 := signatureUtil.Base64ToString(PrivateSignature)
	if err2 != nil {
		res.Message = err2.Error()
		return res, err2
	}

	primaryKey, err3 := signatureUtil.StringToPrimaryKey(privateSignatureString)
	if err3 != nil {
		res.Message = err3.Error()
		return res, err3
	}

	balance, err8 := signatureUtil.Base64ToString(balanceBase64)
	if err8 != nil {
		res.Message = err8.Error()
		return res, err8
	}

	dencrypt, err4 := signatureUtil.Dencrypt(primaryKey, []byte(balance))
	if err4 != nil {
		res.Message = err4.Error()
		return res, err4
	}

	publicSigSmith, err5 := signatureUtil.Base64ToString(smith.PublicSignature)
	if err5 != nil {
		res.Message = err5.Error()
		return res, err5
	}

	publicKey, err6 := signatureUtil.StringToPublicKey(publicSigSmith)
	if err6 != nil {
		res.Message = err6.Error()
		return res, err6
	}

	balanceEncrypt, err7 := signatureUtil.Encrypt(publicKey, dencrypt)
	if err7 != nil {
		res.Message = err7.Error()
		return res, err7
	}

	res.Status = "OK"
	res.Balance = signatureUtil.StringToBase64(string(balanceEncrypt))

	return res, nil
}

func (f *smithInteractor) getBalance(privateSignature string, smith *entities.Smith) (uint64, error) {
	signatureUtil := utils.NewSignatureUtil()
	keyPem, err1 := signatureUtil.Base64ToString(privateSignature)
	if err1 != nil {
		log.Println(err1)
		return 0, err1
	}

	privateKey, err2 := signatureUtil.StringToPrimaryKey(keyPem)
	if err2 != nil {
		log.Println(err2)
		return 0, err2
	}

	balanceString, err4 := signatureUtil.Base64ToString(smith.Wallet.Balance)
	if err4 != nil {
		log.Println(err4)
		return 0, err4
	}

	dencryptBalance, err4 := signatureUtil.Dencrypt(privateKey, []byte(balanceString))
	if err4 != nil {
		log.Println(err4)
		return 0, err4
	}
	balance, _ := strconv.ParseUint(dencryptBalance, 10, 64)
	return balance, nil
}

func (f *smithInteractor) UpdateBalanceSmith(balance uint64, smith *entities.Smith) error {
	signatureUtil := utils.NewSignatureUtil()
	pub, err3 := signatureUtil.Base64ToString(PublicSignature)
	if err3 != nil {
		return err3
	}

	publicKey, err4 := signatureUtil.StringToPublicKey(pub)
	if err4 != nil {
		return err4
	}

	encryptBalance, err5 := signatureUtil.Encrypt(publicKey, strconv.FormatUint(balance, 10))
	if err5 != nil {
		return err5
	}

	smith.Wallet.Balance = signatureUtil.StringToBase64(string(encryptBalance))

	errForg := f.smithCommandRepository.Update(smith)
	if errForg != nil {
		return errForg
	}
	return nil
}

func (f *smithInteractor) blockToBase64(smith *entities.WinnerSmith, blocks []*entities.Block, res *pb.SmithCheckResponse) error {
	signatureUtil := utils.NewSignatureUtil()

	jsonBlock, errJson := json.Marshal(blocks)
	if errJson != nil {
		res.Status = "FAILED"
		res.Message = errJson.Error()
		return errJson
	}

	res.Blocks = signatureUtil.StringToBase64(string(jsonBlock))
	return nil
}

func (f *smithInteractor) epochToBase64(smith *entities.WinnerSmith, epoch *entities.Epoch, res *pb.SmithCheckResponse) error {
	signatureUtil := utils.NewSignatureUtil()

	jsonEpoch, errJson := json.Marshal(epoch)
	if errJson != nil {
		res.Status = "FAILED"
		res.Message = errJson.Error()
		return errJson
	}

	res.Epoch = signatureUtil.StringToBase64(string(jsonEpoch))
	return nil
}

func (f *smithInteractor) signWithBlockchain(res *pb.SmithCheckResponse, block []*entities.Block) error {
	res.PublicSignature = PublicSignature
	signatureUtil := utils.NewSignatureUtil()
	jsonBlock, err := json.Marshal(block)
	if err != nil {
		return err
	}
	privateKeyString, _ := signatureUtil.Base64ToString(PrivateSignature)
	privateKey, _ := signatureUtil.StringToPrimaryKey(privateKeyString)
	sign, errSing := signatureUtil.Sign(privateKey, string(jsonBlock))
	if errSing != nil {
		return err
	}
	res.Signature = signatureUtil.StringToBase64(string(sign))
	return nil
}
