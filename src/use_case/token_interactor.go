package use_case

import (
	"errors"
	"strconv"
	"sync"
	"time"

	"gitlab.com/cewi/blockchain/node/src/entities"
	pb "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
	"gitlab.com/cewi/blockchain/node/src/framework/repository/token"
	"gitlab.com/cewi/blockchain/node/src/utils"
)

type ITokenInteractor interface {
	CreateTokenGenesis(chainId string, hashBlockchain string) (string, error)
	Create(req *pb.TokenRequest) (*pb.TokenResponse, error)
}

var iTokenInteractor ITokenInteractor

type tokenInteractor struct {
	tokenRepository      repository.ITokenCommnadRepository
	tokenQueryRepository repository.ITokenQueryRepository
	tblueInteractor      ITblueInteractor
}

func NewTokenInteractor() ITokenInteractor {
	lock := &sync.Mutex{}
	if iTokenInteractor == nil {
		lock.Lock()
		defer lock.Unlock()
		iTokenInteractor = &tokenInteractor{
			tokenRepository:      repository.NewTokenCommandRepository(),
			tblueInteractor:      NewTblueInteractor(),
			tokenQueryRepository: repository.NewTokenQueryRepository(),
		}
	}
	return iTokenInteractor
}

func (t tokenInteractor) Create(req *pb.TokenRequest) (*pb.TokenResponse, error) {

	res := &pb.TokenResponse{
		Status: "FAIL",
	}

	if req.ChainID == "" {
		res.Message = "chainId not found"
		return res, errors.New(res.Message)
	}

	if req.HashBlockchain == "" {
		res.Message = "hash blockchain not found"
		return res, errors.New(res.Message)
	}

	if req.Nickname == "" {
		res.Message = "nickname not found"
		return res, errors.New(res.Message)
	}

	if req.Acronym == "" {
		res.Message = "acronym not found"
		return res, errors.New(res.Message)
	}

	if req.Balance <= 0 {
		res.Message = "balance invalid"
		return res, errors.New(res.Message)
	}

	tokenFound, _ := t.tokenQueryRepository.FindByNickname(req.Nickname)
	if tokenFound != nil && tokenFound.Nickname != "" {
		res.Message = "nickname already exists"
		return res, errors.New(res.Message)
	}

	tokenFound2, _ := t.tokenQueryRepository.FindByNickname(req.Nickname)
	if tokenFound2 != nil && tokenFound2.Acronym != "" {
		res.Message = "acronym already exists"
		return res, errors.New(res.Message)
	}

	balanceString := strconv.FormatFloat(req.Balance, 'E', -1, 32)

	token := &entities.Token{
		CreatedAt:      time.Now().UTC().String(),
		HashBlockchain: req.HashBlockchain,
		ChainID:        req.ChainID,
		Nickname:       req.Nickname,
		Acronym:        req.Acronym,
		Balance:        balanceString,
	}

	hashToken, err := t.create(token)
	if err != nil {
		res.Message = err.Error()
		return res, err
	}

	res.Status = "OK"
	res.Message = "token created with success"
	res.HashToken = hashToken

	return res, nil

}

func (t tokenInteractor) CreateTokenGenesis(chainId string, hashBlockchain string) (string, error) {
	token := &entities.Token{
		CreatedAt:      time.Now().UTC().String(),
		HashBlockchain: hashBlockchain,
		ChainID:        chainId,
		Nickname:       "Cewi",
		Balance:        "1000000000000000.0",
	}

	hashToken, err := t.create(token)
	if err != nil {
		return "", err
	}
	return hashToken, nil
}

func (t tokenInteractor) create(token *entities.Token) (string, error) {
	signatureUtil := utils.NewSignatureUtil()
	publicSignatureString, err1 := signatureUtil.Base64ToString(PublicSignature)
	if err1 != nil {
		return "", err1
	}

	publicKey, err2 := signatureUtil.StringToPublicKey(publicSignatureString)
	if err2 != nil {
		return "", err2
	}

	encrypt, err3 := signatureUtil.Encrypt(publicKey, token.Balance)
	if err3 != nil {
		return "", err3
	}

	balanceBase64 := signatureUtil.ByteToBase64(encrypt)

	token.GenerateHash()
	token.Balance = balanceBase64
	hashToken, errToken := t.tokenRepository.Create(token)
	if errToken != nil {
		return "", errToken
	}

	_, errTblue := t.tblueInteractor.Create(hashToken, token.ChainID, token.HashBlockchain, balanceBase64)
	if errTblue != nil {
		return "", errTblue
	}
	return hashToken, nil
}
