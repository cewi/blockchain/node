package use_case

import (
  "sync"
  "time"

  "gitlab.com/cewi/blockchain/node/src/entities"
  "gitlab.com/cewi/blockchain/node/src/framework/repository"
)

type ITransactionInteractor interface {
	CreateTrasactionGenesis(hashEpoch string, hashBlock string) (string, error)
}

var itransactionInteractor ITransactionInteractor

type transactionInteractor struct {
	transactionRepository repository.ITransactionCommandRepository
}

func NewTransactionInteractor() ITransactionInteractor {
	lock := &sync.Mutex{}
	if itransactionInteractor == nil {
		lock.Lock()
		defer lock.Unlock()
		itransactionInteractor = &transactionInteractor{
			transactionRepository: repository.NewTransactionCommandRepository(),
		}
	}
	return itransactionInteractor
}

func (t transactionInteractor) CreateTrasactionGenesis(hashEpoch string, hashBlock string) (string, error) {
	transaction := &entities.Transaction{
		HashEpoch: hashEpoch,
		HashBlock: hashBlock,
		CreatedAt: time.Now().UTC().String(),
		Amount:    0,
	}
	transaction.GenerateHash()
	return t.transactionRepository.Create(transaction)
}
