package use_case

import (
	"sync"

	"gitlab.com/cewi/blockchain/node/src/entities"
	pb "gitlab.com/cewi/blockchain/node/src/framework/proto_buffer"
	repository "gitlab.com/cewi/blockchain/node/src/framework/repository/wallet"
	"gitlab.com/cewi/blockchain/node/src/utils"
)

type IWalletInteractor interface {
	Create(req *pb.WalletRequest) (*pb.WalletReponse, error)
	NewAddress() (*pb.AddressResponse, error)
	GetBalance(req *pb.BalanceRequest) (*pb.BalanceResponse, error)
}

var iWalletInteractor IWalletInteractor

type walletInteractor struct {
	walletRepository      repository.IWalletCommandRepository
	walletQueryRepository repository.IWalletQueryRepository
}

func NewWalletInteractor() IWalletInteractor {
	lock := &sync.Mutex{}
	if iWalletInteractor == nil {
		lock.Lock()
		defer lock.Unlock()
		iWalletInteractor = &walletInteractor{
			walletRepository:      repository.NewWalletCommandRepository(),
			walletQueryRepository: repository.NewWalletQueryRepository(),
		}
	}
	return iWalletInteractor
}

func (w *walletInteractor) Create(req *pb.WalletRequest) (*pb.WalletReponse, error) {
	res := &pb.WalletReponse{
		Status: "FAIL",
	}

	signatureUtil := utils.NewSignatureUtil()
	pub, err3 := signatureUtil.Base64ToString(req.PublicSignatureBase64)
	if err3 != nil {
		res.Message = err3.Error()
		return res, err3
	}

	publicKey, err4 := signatureUtil.StringToPublicKey(pub)
	if err4 != nil {
		res.Message = err4.Error()
		return res, err4
	}

	encryptBalance, err5 := signatureUtil.Encrypt(publicKey, "0")
	if err5 != nil {
		res.Message = err5.Error()
		return res, err5
	}

	encryptWallet, err6 := signatureUtil.Encrypt(publicKey, req.Address)
	if err6 != nil {
		res.Message = err6.Error()
		return res, err6
	}

	var myTokens []*entities.MyTokens

	hash, err := w.walletRepository.Create(
		&entities.Wallet{
			Address:  signatureUtil.StringToBase64(string(encryptWallet)),
			Balance:  signatureUtil.StringToBase64(string(encryptBalance)),
			MyTokens: myTokens,
		},
	)

	if err != nil {
		res.Message = err.Error()
		return res, err
	}

	res.Status = "OK"
	res.Hash = hash

	return res, nil
}

func (w *walletInteractor) GetBalance(req *pb.BalanceRequest) (*pb.BalanceResponse, error) {
	res := &pb.BalanceResponse{
		Status: "FAIL",
	}

	signatureUtil := utils.NewSignatureUtil()

	wallet, err7 := w.walletQueryRepository.FindByOne(req.Hash)
	if err7 != nil {
		res.Message = err7.Error()
		return res, err7
	}

	key, err8 := signatureUtil.Base64ToString(req.PrivateSignatureBase64)
	if err8 != nil {
		res.Message = err8.Error()
		return res, err8
	}

	privateKey, err9 := signatureUtil.StringToPrimaryKey(key)
	if err9 != nil {
		res.Message = err9.Error()
		return res, err9
	}

	balanceStrig, err10 := signatureUtil.Base64ToString(wallet.Balance)
	if err10 != nil {
		res.Message = err10.Error()
		return res, err10
	}

	balance, err11 := signatureUtil.Dencrypt(privateKey, []byte(balanceStrig))
	if err11 != nil {
		res.Message = err11.Error()
		return res, err11
	}

	res.Status = "OK"
	res.Balance = balance

	return res, nil
}

func (w *walletInteractor) NewAddress() (*pb.AddressResponse, error) {
	addressUtils := utils.NewAddressUtils()

	res := &pb.AddressResponse{
		Status:  "OK",
		Message: "new address has been generated",
		Address: addressUtils.RandAddress(),
	}

	return res, nil
}
