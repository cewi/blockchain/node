package utils

import (
	"fmt"
	"math/rand"
	"sync"
)

type IAddressUtils interface {
	RandAddress() string
}

type addressUtils struct {
}

var iAddressUtils IAddressUtils

func NewAddressUtils() IAddressUtils {
	lock := &sync.Mutex{}
	if iAddressUtils == nil {
		lock.Lock()
		defer lock.Unlock()
		iAddressUtils = &addressUtils{}
	}
	return iAddressUtils
}

func (a addressUtils) RandAddress() string {
	b := make([]byte, 32)
	_, _ = rand.Read(b)
	return fmt.Sprintf("%x", b)
}
