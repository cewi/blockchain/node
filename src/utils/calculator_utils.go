package utils

import "sync"

type ICalculatorUtils interface {
  CalculateRate(amount float64, txt float64) (float64, float64)
}

type calculatorUtils struct {
}

var calculator *calculatorUtils

func NewCalculator() ICalculatorUtils {
  lock := &sync.Mutex{}
  if calculator == nil {
    lock.Lock()
    defer lock.Unlock()
    calculator = &calculatorUtils{}
  }
  return calculator
}

func (c *calculatorUtils) CalculateRate(amount float64, txt float64) (float64, float64) {
  rate := amount * (txt / 100)
  amountRate := amount + rate
  return amountRate, rate
}
