package utils

import (
  "github.com/go-co-op/gocron"
  "log"
  "time"
)

func RunCronJobs(cron string, f func()) {
  go run(cron, f)
}

func run(cron string, f func()) {
  s := gocron.NewScheduler(time.UTC)
  _, err := s.Every(1).Cron(cron).Do(f)
  if err != nil {
    log.Println(err)
    return
  }
  s.StartBlocking()
}
