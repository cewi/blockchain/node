package utils

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	"crypto/x509"
	b64 "encoding/base64"
	"encoding/pem"
	"log"
	"strings"
	"sync"
)

type SignatureUtil struct {
}

var signatureUtil *SignatureUtil

func NewSignatureUtil() *SignatureUtil {
	lock := &sync.Mutex{}
	if signatureUtil == nil {
		lock.Lock()
		defer lock.Unlock()
		signatureUtil = &SignatureUtil{}
	}
	return signatureUtil
}

func (s *SignatureUtil) ByteToString(b []byte) string {
	value := make([]string, len(b))
	for i := range b {
		value[i] = string(b[i])
	}
	return strings.Join(value, "")
}

func (s *SignatureUtil) StringToPrimaryKey(keyPEM string) (*rsa.PrivateKey, error) {
	block, _ := pem.Decode([]byte(keyPEM))
	return x509.ParsePKCS1PrivateKey(block.Bytes)
}

func (s *SignatureUtil) StringToPublicKey(pubPEM string) (*rsa.PublicKey, error) {
	block, _ := pem.Decode([]byte(pubPEM))
	return x509.ParsePKCS1PublicKey(block.Bytes)
}

func (s *SignatureUtil) StringToBase64(certificate string) string {
	return b64.RawURLEncoding.EncodeToString([]byte(certificate))
}

func (s *SignatureUtil) Base64ToString(base64 string) (string, error) {
	decode, err := b64.RawURLEncoding.DecodeString(base64)
	return string(decode), err
}

func (s *SignatureUtil) Sign(key *rsa.PrivateKey, value string) ([]byte, error) {
	_, msgHashSum := NewHashUtils().Generate(value)
	return rsa.SignPSS(rand.Reader, key, crypto.SHA512, msgHashSum, nil)
}

func (s *SignatureUtil) VerifyPSS(pub *rsa.PublicKey, value string, signature []byte) bool {
	_, msgHashSum := NewHashUtils().Generate(value)
	err := rsa.VerifyPSS(pub, crypto.SHA512, msgHashSum, signature, nil)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}

func (s *SignatureUtil) Dencrypt(key *rsa.PrivateKey, encryptedBytes []byte) (string, error) {
	decryptedBytes, err := key.Decrypt(nil, encryptedBytes, &rsa.OAEPOptions{Hash: crypto.SHA512})
	if err != nil {
		log.Println(err)
		return "", err
	}
	return string(decryptedBytes), nil
}
func (s *SignatureUtil) Encrypt(pub *rsa.PublicKey, value string) ([]byte, error) {
	encryptedBytes, err := rsa.EncryptOAEP(sha512.New(), rand.Reader, pub, []byte(value), nil)
	if err != nil {
		return nil, err
	}
	return encryptedBytes, nil
}

func (s *SignatureUtil) ByteToBase64(encrypt []byte) string {
	return s.StringToBase64(s.ByteToString(encrypt))
}
